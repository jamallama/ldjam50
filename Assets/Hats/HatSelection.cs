using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatSelection : MonoBehaviour {

    public static HatSelection Instance;

    public FXController PenguinBounceFX;
    
    public const string SelectedHatKey = "Selected Hat";
    public const string NoHatValue = "NoHat";

    public static Action NewHatSelected;

    public FXController ShimmyFX;
    public float ShimmyTime;
    public float ShimmyAmp;
    private float _shimmyTimer;
    private float _lastShimmy = 1f;

    public void Awake() {
        Instance = this;
    }
    
    public static string GetSelectedHatID() {
        return PlayerPrefs.GetString(SelectedHatKey, NoHatValue);
    }

    public static void SetSelectedHatID(string newHatID) {
        bool newHat = newHatID != GetSelectedHatID();
        PlayerPrefs.SetString(SelectedHatKey, newHatID);
        if (newHat) {
            NewHatSelected?.Invoke();
            Instance.PenguinBounceFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
        }
    }

    public void PenguinClicked() {
        PenguinBounceFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
        
        // SFX
        AudioController.Instance.SqueakyToySFX(gameObject);
    }

    private void Update() {
        _shimmyTimer += Time.deltaTime;
        if (_shimmyTimer > ShimmyTime) {
            _shimmyTimer = 0f;
            _lastShimmy *= -1f;
            ShimmyFX.TriggerWithArgs(new FXArgs {Amplitude = ShimmyAmp * _lastShimmy, Speed = 1f / ShimmyTime});
        }
    }
    
}
