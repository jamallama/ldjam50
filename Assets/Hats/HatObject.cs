using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
[CreateAssetMenu(fileName="New Hat", menuName="Items/Hat")]
public class HatObject : ScriptableObject {

  public string ID;
  public string Name;
  public GameObject prefab;
  public Sprite Sprite;
  public Sprite MenuSpriteIfSelected;
  [TextArea(5, 10)]
  public string Story;

}
