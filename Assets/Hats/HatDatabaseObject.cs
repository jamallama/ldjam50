using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName="New Hat Database", menuName="Items/Hat Database")]
public class HatDatabaseObject : ScriptableObject {

    public HatObject[] Hats;

}
