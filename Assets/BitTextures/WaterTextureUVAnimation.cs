using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class WaterTextureUVAnimation : MonoBehaviour {

	[Range(-1, 1)]
	public float xDir = 0;
	[Range(-1, 1)]
	public float yDir = 1;
	public float speed = 1.0f;
	public float tiling = 16.0f;
	public Color tintColor = Color.white;
	public float rotationAngle = 1.0f;


	private MeshRenderer renderer;
	private Material material;



	void Awake() {
		renderer = GetComponent<MeshRenderer>();
		material = new Material(renderer.material);
		renderer.material = material;
		renderer.material.mainTextureScale = new Vector2(tiling, tiling);
		renderer.material.color = tintColor;
	}



	void Update() {
		renderer.material.mainTextureOffset += new Vector2(xDir, yDir) * speed * Time.deltaTime;
		if (rotationAngle != 0.0f) {
			transform.RotateAround(transform.position, Vector2.up, rotationAngle);
		}
	}



}
