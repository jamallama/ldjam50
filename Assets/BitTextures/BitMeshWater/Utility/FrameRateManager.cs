﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class FrameRateManager : MonoBehaviour {
	
	public enum VSyncOptions {Off, OneFrame, TwoFrames, ThreeFrames, FourFrames};
	public int targetFramerate;
	public VSyncOptions vSyncCount;

	private int vSync;

	void Start () {

		if (vSyncCount == VSyncOptions.Off) { vSync = -1; }
		if (vSyncCount == VSyncOptions.OneFrame) { vSync = 1; }
		if (vSyncCount == VSyncOptions.TwoFrames) { vSync = 2; }
		if (vSyncCount == VSyncOptions.ThreeFrames) { vSync = 3; }
		if (vSyncCount == VSyncOptions.FourFrames) { vSync = 4; }

		QualitySettings.vSyncCount = vSync;

		Application.targetFrameRate = targetFramerate;
	}

}
