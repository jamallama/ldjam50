﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MeshGenerator : MonoBehaviour
{
	// public enum VertexResolution { Sixteenth, Eighth, Quarter, Half, Full };
	public Vector2Int meshDimensions;
	public int gridSize;
	public int resolution = 1;
	public Material material;
	public float animationSpeed = 0.06f;
	public Vector2 offsetAmountPerLoop = new Vector2(0.1f, 0);
	public float additionalZOffset = -0.3f;
	public bool loopAnimation = true;
	public AnimationCurve heightCurve;
	public float heightMultiplier = 16.0f;
	public bool useReflections = true;

	[Header("Noise Values")]
	public int noiseScale = 16;
	public int noiseOctvaes = 4;
	public float noisePersistence = 1.0f;
	public float noiseLacunarity = 0.5f;
	

	private Mesh msh;
	private MeshFilter meshFilter;
	[HideInInspector] public MeshRenderer meshRenderer;

	private float[,] heightMap;
	private Vector3[] vertices;
	private Vector2[] uvs;
	private Vector2[] points;
	private Vector2 currentOffset = Vector2.zero;



	private void Start() {
		StartCoroutine(UpdateMesh(offsetAmountPerLoop));
	}



	private IEnumerator UpdateMesh(Vector2 offset) {
		// yield return new WaitForSeconds(Time.deltaTime * animationSpeed);
		float progress = 0.0f;
		currentOffset += offset;
		while (progress < animationSpeed) {
			progress += Time.deltaTime;
			yield return null;
		}
		UpdateVertices(currentOffset);
		if (loopAnimation) { StartCoroutine(UpdateMesh(offset)); }
	}




	public void UpdateVertices(Vector2 offset) {
		if (heightMap != null) { Array.Clear(heightMap, 0, heightMap.Length); }
		if (points != null) { Array.Clear(points, 0, points.Length); }

		int width = meshDimensions.x * resolution;
		int height = meshDimensions.y * resolution;
		points = new Vector2[width * height];
		// heightMap = new float[width, height];
		heightMap = GenerateNoiseMap(0, width, height, noiseScale, noiseOctvaes, noisePersistence, noiseLacunarity, offset);

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				// heightMap[x, y] = UnityEngine.Random.Range(0, gridSize + 1);
				Vector2 newPos = new Vector2(x, y) * gridSize;
				points[x + y * width] = newPos;
			}
		}
		GenerateMesh(points, heightMap, material);
	}


	private void SetupReflections(MeshRenderer renderer) {
		renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		renderer.receiveShadows = true;
		// renderer.receiveGI = ReceiveGI.LightProbes;

		renderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.BlendProbes;
		renderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.BlendProbes;
	}



	public void GenerateMesh(Vector2[] points, float[,]heightMap, Material meshMaterial) {
		int width = meshDimensions.x * resolution;
		int height = meshDimensions.y * resolution;

		if (vertices != null) { Array.Clear(vertices, 0, vertices.Length); }
		vertices = new Vector3[points.Length];

		if (uvs != null) { Array.Clear(uvs, 0, uvs.Length); }
		uvs = new Vector2[points.Length];

		int[] triangles = new int[width * height * 6]; // Area * 2 (2 triangles per square) * 3 (3 indices in the vertex array, per triangle)
		int triIndex = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int index = x + y * width;
				vertices[index] = new Vector3(points[index].x, points[index].y, heightCurve.Evaluate(heightMap[x, y]) * (1.0f / gridSize) * heightMultiplier + additionalZOffset);
				uvs[index] = new Vector2(x / (float)width, y / (float)height);

				// We should only need to define the triangles if the mesh hasn't been created yet
				if (x < width - 1 && y < height - 1) {
					if ((x + y) % 2 == 0) {
						//   |\|
						triangles[triIndex] = index; // bottom left
						triangles[triIndex + 1] = index + width; // top left
						triangles[triIndex + 2] = index + 1; // bottom right
						triangles[triIndex + 3] = index + 1; // bottom right
						triangles[triIndex + 4] = index + width; // top left
						triangles[triIndex + 5] = index + width + 1; // top right
					} else {
						//   |/|
						triangles[triIndex] = index; // bottom left
						triangles[triIndex + 1] = index + width; // top left
						triangles[triIndex + 2] = index + width + 1; // top right
						triangles[triIndex + 3] = index; // bottom left
						triangles[triIndex + 4] = index + width + 1; // top right
						triangles[triIndex + 5] = index + 1; // bottom right
					}
				}
				// Add six, since each pass adds six vertices to the array
				triIndex += 6;
			}
		}

		// Create the mesh if the createMesh bool is true,
		// otherwise we're just updating the existing mesh vertex positions.
		if (msh != null) { msh.Clear(false); }
		if (msh == null) { msh = new Mesh(); }
		
		msh.vertices = vertices;
		// Triangles are an array of vertex indices, so 0, 1, 2 should describe the vertices which make up a triangle in the mesh's vertex array, based on the indices
		msh.triangles = triangles;
		msh.uv = uvs;
		msh.RecalculateNormals();
		msh.RecalculateBounds();

		// Set up game object with mesh;
		meshRenderer = meshRenderer == null ? gameObject.AddComponent<MeshRenderer>() : meshRenderer;
		if (useReflections) { SetupReflections(meshRenderer); }
		meshFilter = meshFilter == null ? meshFilter = gameObject.AddComponent<MeshFilter>() : meshFilter;
		meshFilter.mesh = msh;

		meshRenderer.material = meshMaterial;
	}



	// Generate a map using Perlin noise
	public static float[,] GenerateNoiseMap(int seed, int mapWidth, int mapHeight, float scale, int octaves, float persistance, float lacunarity, Vector2 offset) {
		float[,] noiseMap = new float[mapWidth, mapHeight];
		System.Random prng = new System.Random(seed);
		Vector2[] octaveOffsets = new Vector2[octaves];
		for (int i = 0; i < octaves; i++) {
			float offsetX = prng.Next(-100000, 100000) + offset.x;
			float offsetY = prng.Next(-100000, 100000) + offset.y;
			octaveOffsets[i] = new Vector2(offsetX, offsetY);
		}
		scale = scale <= 0 ? 0.0001f : scale;
		float maxNoiseHeight = float.MinValue;
		float minNoiseHeight = float.MaxValue;
		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;
		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				float amplitude = 1;
				float frequency = 1;
				float noiseHeight = 0;
				for (int i = 0; i < octaves; i++) {
					float sampleX = (x - halfWidth) / scale * frequency + octaveOffsets[i].x;
					float sampleY = (y - halfHeight) / scale * frequency + octaveOffsets[i].y;
					float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
					noiseHeight += perlinValue * amplitude;
					amplitude *= persistance;
					frequency *= lacunarity;
				}
				if (noiseHeight > maxNoiseHeight) {
					maxNoiseHeight = noiseHeight;
				} else if (noiseHeight < minNoiseHeight) {
					minNoiseHeight = noiseHeight;
				}
				noiseMap[x, y] = noiseHeight;
			}
		}
		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
			}
		}
		return noiseMap;
	}
}
