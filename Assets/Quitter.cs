using UnityEngine;

public class Quitter : MonoBehaviour
{
    public void Quit() {
        if (Application.isEditor) {
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #endif
        } else {
            Application.Quit();
        }
    }
}
