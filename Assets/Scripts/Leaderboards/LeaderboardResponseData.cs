using System;


// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedField.Local

[Serializable]
public class LeaderboardResponseData {
    public string entryType;
    public long server_time;
    public LeaderboardEntry[] leaderboard;
    public int versionId;
    public string leaderboardId;
    public bool moreAfter;
    public bool moreBefore;
}

[Serializable]
public class LeaderboardEntry {
    public string playerId;
    public long score;
    public ScoreMetadata data;
    public long createdAt;
    public long updatedAt;
    public int index;
    public int rank;
    public string name;
    public object summaryFriendData;
    public string pictureUrl;
}
// ReSharper restore NotAccessedField.Local
// ReSharper restore InconsistentNaming
