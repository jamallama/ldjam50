using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScoreSubmission : MonoBehaviour {
    private bool _submitAfterAuthentication;
    
    public GlobalVariable Time;
    public GlobalVariable Deaths;
    
    public TMP_InputField Name;
    public TMP_InputField Score;
    
    public static event Action OnScoreSubmitted;

    public UnityEvent OnAuthenticated;
    public UnityEvent OnScoreSubmitSuccess;
    public UnityEvent OnStartSubmitScore;
    public UnityEvent OnScoreSubmitFailure;

    private void Start() {
        GameServicesWrapper.AuthenticateIfNecessary();
        OnStartSubmitScore.Invoke();
        GameServicesWrapper.OnAuthenticated += Authenticated;
        GameServicesWrapper.OnAuthenticationFailed += AuthenticationFailed;
        Time.ValueChanged += UpdateScore;
        UpdateScore(Time.Value);
    }

    private void OnDestroy() {
        GameServicesWrapper.OnAuthenticated -= Authenticated;
        GameServicesWrapper.OnAuthenticationFailed -= AuthenticationFailed;
        Time.ValueChanged -= UpdateScore;
    }

    private void UpdateScore(float value) {
        Score.text = ((int) (value * 1000)).ToString();
    }

    private void AuthenticationFailed() {
        _submitAfterAuthentication = true;
        OnScoreSubmitFailure.Invoke();
    }

    private void Authenticated(AuthenticateResponseData authenticateResponseData) {
        Name.text = authenticateResponseData.playerName;
        OnAuthenticated.Invoke();
        if (_submitAfterAuthentication) {
            _submitAfterAuthentication = false;
            Submit();
        }
    }

    public void Submit() {
        if (!long.TryParse(Score.text, out var score)) return;
        if (string.IsNullOrWhiteSpace(Name.text)) return;
        
        OnStartSubmitScore.Invoke();
        if (_submitAfterAuthentication && GameServicesWrapper.AuthenticateIfNecessary()) {
            return;
        }
        GameServicesWrapper.UpdateName(Name.text);
        string metadata = JsonUtility.ToJson(new ScoreMetadata {
            Deaths = Mathf.RoundToInt(Deaths.Value)
        });
        GameServicesWrapper.BCW.LeaderboardService.PostScoreToLeaderboard("time", score, metadata, ScoreSubmitted, ScoreSubmissionFailed);
    }

    private void ScoreSubmissionFailed(int status, int reasoncode, string jsonerror, object cbobject) {
        OnScoreSubmitFailure.Invoke();
        _submitAfterAuthentication = true;
    }

    private void ScoreSubmitted(string jsonresponse, object cbobject) {
        OnScoreSubmitted?.Invoke();
        OnScoreSubmitSuccess.Invoke();
    }
}
