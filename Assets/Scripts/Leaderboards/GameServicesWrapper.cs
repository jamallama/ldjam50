using System;
using UnityEngine;

[RequireComponent(typeof(BrainCloudWrapper))]
public class GameServicesWrapper : MonoBehaviour {
    private static GameServicesWrapper _instance;
    public static BrainCloudWrapper BCW
    {
        get
        {
            if (_instance == null) {
                _instance = new GameObject("GameServicesWrapper", typeof(BrainCloudWrapper), typeof(GameServicesWrapper)).GetComponent<GameServicesWrapper>();
            }
            return _instance._bcw;
        }
    }

    private static AuthenticateResponseData _lastAuthenticatedResponse;
    private static bool _lastAuthenticationFailed;
    private static bool _authenticating;
    private static event Action<AuthenticateResponseData> OnAuthenticatedInternal;
    public static event Action<AuthenticateResponseData> OnAuthenticated
    {
        add
        {
            OnAuthenticatedInternal += value;
            if (BCW.Client.Authenticated) {
                value(_lastAuthenticatedResponse);
            }
        }
        remove => OnAuthenticatedInternal -= value;
    }
    private static event Action OnAuthenticationFailedInternal;
    public static event Action OnAuthenticationFailed
    {
        add
        {
            OnAuthenticationFailedInternal += value;
            if (_lastAuthenticationFailed) {
                value();
            }
        }
        remove => OnAuthenticationFailedInternal -= value;
    }
    
    private BrainCloudWrapper _bcw;
    
    void Awake() {
        if (_instance != null) {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        _bcw = GetComponent<BrainCloudWrapper>();
        DontDestroyOnLoad(gameObject);
        if (!_bcw.Client.Initialized) {
            _bcw.Init();
            Authenticate();
        }
    }

    public static bool AuthenticateIfNecessary() {
        if (!BCW.Client.IsAuthenticated() && !_authenticating) {
            _instance.Authenticate();
            return true;
        }
        return false;
    }

    private void Authenticate() {
        _authenticating = true;
        _lastAuthenticatedResponse = null;
        _lastAuthenticationFailed = false;
        _bcw.AuthenticateAnonymous(OnAuthenticate, OnAuthenticateFail);
    }

    private void OnAuthenticateFail(int status, int reasoncode, string jsonerror, object cbobject) {
        Debug.Log("Auth failure");
        _lastAuthenticationFailed = true;
        _authenticating = false;
        OnAuthenticationFailedInternal?.Invoke();
    }

    private void OnAuthenticate(string jsonresponse, object cbobject) {
        Debug.Log("Auth success");
        _lastAuthenticatedResponse = JsonUtility.FromJson<BrainCloudResponse<AuthenticateResponseData>>(jsonresponse).data;
        _authenticating = false;
        OnAuthenticatedInternal?.Invoke(_lastAuthenticatedResponse);
    }

    public static void UpdateName(string name) {
        BCW.PlayerStateService.UpdateName(name, OnUpdateName);
    }

    private static void OnUpdateName(string jsonresponse, object cbobject) {
        var updateData = JsonUtility.FromJson<BrainCloudResponse<AuthenticateResponseData>>(jsonresponse).data;
        _lastAuthenticatedResponse.playerName = updateData.playerName;
    }
}
