using System;
using TMPro;
using UnityEngine;

public class LeaderboardRow : MonoBehaviour {
    public TextMeshProUGUI Place;
    public TextMeshProUGUI Name;
    public TextMeshProUGUI Score;
    public TextMeshProUGUI Deaths;
    
    public void SetScore(LeaderboardEntry entry) {
        Place.text = entry.rank.ToString();
        Name.text = string.IsNullOrWhiteSpace(entry.name) ? "Some guy" : entry.name;
        Score.text = TimeSpan.FromMilliseconds(entry.score).ToString(@"mm\:ss\.fff");
        Deaths.text = entry.data.Deaths.ToString();
    }
}
