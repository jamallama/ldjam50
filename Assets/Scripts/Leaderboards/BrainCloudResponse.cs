using System;

[Serializable]
public class BrainCloudResponse<TData> {
    public string status_message;
    public int reason_code;
    public TData data;
    public int status;
}