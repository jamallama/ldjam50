using System;
using BrainCloud;
using UnityEngine;
using UnityEngine.Events;

public class LeaderboardDisplay : MonoBehaviour {
    private static LeaderboardEntry[] _topPlayers = Array.Empty<LeaderboardEntry>();
    private static LeaderboardEntry[] _playerView = Array.Empty<LeaderboardEntry>();

    public bool Global;

    public UnityEvent OnStartLoad;
    public UnityEvent OnLoaded;
    public UnityEvent OnLoadFailed;
    
    public void Start() {
        ShowLoading();
        GameServicesWrapper.OnAuthenticated += OnAuthenticated;
        GameServicesWrapper.OnAuthenticationFailed += OnLoadFailed.Invoke;
        ScoreSubmission.OnScoreSubmitted += ScoreSubmitted;
    }
    
    private void OnDestroy() {
        GameServicesWrapper.OnAuthenticated -= OnAuthenticated;
        GameServicesWrapper.OnAuthenticationFailed -= OnLoadFailed.Invoke;
        ScoreSubmission.OnScoreSubmitted -= ScoreSubmitted;
    }

    private void ScoreSubmitted() {
        LoadLeaderboard();
    }

    private void OnAuthenticated(AuthenticateResponseData authenticateResponseData) {
        LoadLeaderboard();
    }

    private void LoadLeaderboard() {
        ShowLoading();
        if (Global) {
            GameServicesWrapper.BCW.LeaderboardService.GetGlobalLeaderboardPage("time",
                BrainCloudSocialLeaderboard.SortOrder.HIGH_TO_LOW, 0, 9, LeaderboardLoaded, LeaderboardLoadFailed);
        } else {
            GameServicesWrapper.BCW.LeaderboardService.GetGlobalLeaderboardView("time",
                BrainCloudSocialLeaderboard.SortOrder.HIGH_TO_LOW, 5, 4, LeaderboardLoaded, LeaderboardLoadFailed);
        }
    }

    private void ShowLoading() {
        OnStartLoad.Invoke();
    }

    private void ShowFailure() {
        OnLoadFailed.Invoke();
    }

    private void LeaderboardLoadFailed(int status, int reasonCode, string jsonError, object cbObject) {
        OnLoadFailed.Invoke();
    }
    
    private void LeaderboardLoaded(string jsonresponse, object cbobject) {
        var responseObj = JsonUtility.FromJson<BrainCloudResponse<LeaderboardResponseData>>(jsonresponse);
        if (Global) {
            _topPlayers = responseObj.data.leaderboard;
        } else {
            _playerView = responseObj.data.leaderboard;
        }
        PopulateLeaderboard();
    }

    
    private void PopulateLeaderboard() {
        var leaderboardEntries = Global ? _topPlayers : _playerView;
        var rows = GetComponentsInChildren<LeaderboardRow>(true);
        int i;
        for (i = 0; i < leaderboardEntries.Length; ++i) {
            LeaderboardRow row;
            if (i >= rows.Length) {
                row = Instantiate(rows[0], rows[0].transform.parent);
            } else {
                row = rows[i];
            }
            row.SetScore(leaderboardEntries[i]);
            row.gameObject.SetActive(true);
        }

        for (; i < rows.Length; ++i) {
            rows[i].gameObject.SetActive(false);
        }
        
        OnLoaded.Invoke();
    }
}