using System;
using UnityEngine;

[CreateAssetMenu]
public class GlobalVariable : ScriptableObject {
    [SerializeField] private float _initialValue;
    [SerializeField] private float _maxValue = Single.PositiveInfinity;
    [SerializeField] private float _minValue = Single.NegativeInfinity;
    public float InitialValue => _initialValue;
    public float MaxValue => _maxValue;
    public float MinValue => _minValue;

    private float _value;

    public event Action<float> ValueChanged;
    public event Action ReachedMinimum;
    public event Action ReachedMaximum;

    private void OnEnable() {
        _value = _initialValue - 1;
        Value = InitialValue;
    }

    public float Value {
        get => _value;
        set {
            float newValue = Mathf.Clamp(value, _minValue, _maxValue);
            // ReSharper disable once CompareOfFloatsByEqualityOperator Don't worry about it
            if (_value == newValue) {
                return;
            }
            _value = newValue;
            ValueChanged?.Invoke(value);
            if (_value <= _minValue) {
                ReachedMinimum?.Invoke();
            } else if (_value >= _maxValue) {
                ReachedMaximum?.Invoke();
            }
        }
    }

    public void Add(float addend) {
        Value += addend;
    }

    public void Multiply(float multiplicand) {
        Value *= multiplicand;
    }
}
