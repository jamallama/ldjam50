using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarColorizer : MonoBehaviour {

    public Image ColorImage;
    public Image FillImage;
    public Gradient ColorByFill;

    [Header("Flashing")]
    public float FlashThreshold;
    public float TimeBetweenFlashes;
    public AnimationCurve FlashStrength;
    public FXController AttentionFX;

    private float _timeLastFlashStarted;

    private float _lastFillAmount;
    
    private void Update() {
        float fillAmount = FillImage.fillAmount;
        Color gradientColor = ColorByFill.Evaluate(fillAmount);
        float timeSinceLastFlashStarted = Time.time - _timeLastFlashStarted;

        if (fillAmount < FlashThreshold) {
            if (timeSinceLastFlashStarted > TimeBetweenFlashes) {
                _timeLastFlashStarted = Time.time;
                AttentionFX.TriggerWithArgs(new FXArgs {Amplitude = Mathf.Lerp(.2f, .5f, (FlashThreshold - fillAmount) / FlashThreshold)});
            }
        }

        if (_lastFillAmount - fillAmount > 0.01f) {
            float damageTaken = _lastFillAmount - fillAmount;
            AttentionFX.TriggerWithArgs(new FXArgs {Amplitude = Mathf.Lerp(1f, 3f, damageTaken / 0.125f)});
        }

        if (fillAmount - _lastFillAmount > 0.01f) {
            _timeLastFlashStarted = Time.time;
        }

        gradientColor = gradientColor.ReplacePortion(Color.white, FlashStrength.Evaluate(timeSinceLastFlashStarted));
        ColorImage.color = gradientColor;

        
        _lastFillAmount = fillAmount;
    }

}
