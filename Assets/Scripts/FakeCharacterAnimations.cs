﻿using System.Collections.Generic;
using UnityEngine;

public class FakeCharacterAnimations : MonoBehaviour {

    public PlayerController Controller;
    
    private Transform myTransform;
    public Transform SpriteTransform;
    
    public FXController StepRightFX;
    public FXController StepLeftFX;

    public float DistancePerStep;
    private float distanceTravelled;

    private Vector3 lastPosition;
    private bool lastStepWasLeft;

    private bool facingLeft = true;

    public FXController JumpFX;
    public FXController DoubleJumpRightFX;
    public FXController DoubleJumpLeftFX;

    public SpriteRenderer SpriteRenderer;
    public Sprite NormalSprite;
    public Sprite JumpSprite1;
    public Sprite JumpSprite2;
    public float JumpTimePerSprite;
    private bool _doingAirSprite;
    private float _airTime;
    
    private void Start() {
        myTransform = transform;
        lastPosition = myTransform.position;
        distanceTravelled = DistancePerStep - 0.01f;
        Controller.BecomesGroundedAction += OnBecomesGrounded;
        Controller.StartsJumpAction += DoJumpFX;
        Controller.StartsDoubleJumpAction += DoDoubleJumpFX;
        Controller.BecomesGroundedAction += DoGroundedFX;
    }

    private void OnBecomesGrounded() {
        distanceTravelled = DistancePerStep - 0.01f;
    }
    
    private void Update() {
        
        if (/*Controller.State.IsGrounded*/ true) {
            if (Controller.IsGrounded) {
                float xDiff = myTransform.position.x - lastPosition.x;
                float zDiff = myTransform.position.z - lastPosition.z;
                distanceTravelled += new Vector2(xDiff, zDiff).magnitude;
            }
            if (distanceTravelled > DistancePerStep) {
                distanceTravelled = 0f;
            
                if (lastStepWasLeft) {
                    StepRightFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
                }
                else {
                    StepLeftFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
                }
                lastStepWasLeft = !lastStepWasLeft;
            }
        }
        else {
            distanceTravelled = 1f;
        }

        bool travelledLeft = (myTransform.position - lastPosition).x < -0.025f;
        bool travelledRight = (myTransform.position - lastPosition).x > 0.025f;
        Vector3 spriteScale = SpriteTransform.localScale;
        if (facingLeft && travelledRight) {
            SpriteTransform.localScale = new Vector3(-spriteScale.x, spriteScale.y, spriteScale.z);
            facingLeft = false;
        } else if (!facingLeft && travelledLeft) {
            SpriteTransform.localScale = new Vector3(-spriteScale.x, spriteScale.y, spriteScale.z);
            facingLeft = true;
        }

        if (_doingAirSprite) {
            _airTime += Time.deltaTime;
            SpriteRenderer.sprite = ((int) (_airTime / JumpTimePerSprite)) % 2 == 0 ? JumpSprite1 : JumpSprite2;
        }
        

        lastPosition = myTransform.position;
    }

    public void DoGroundedFX() {
        _doingAirSprite = false;
        SpriteRenderer.sprite = NormalSprite;
    }

    public void DoJumpFX() {
        _airTime = 0f;
        _doingAirSprite = true;
        SpriteRenderer.sprite = JumpSprite1;
        JumpFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
    }

    public void DoDoubleJumpFX() {
        if (facingLeft) {
            DoubleJumpLeftFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
        }
        else {
            DoubleJumpRightFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
        }
    }

}
