using Rewired;
using UnityEngine;

public class GameController : MonoBehaviour {
    public const string HasCompletedAtLeastOneGame = "HasCompletedAtLeastOneGame";
    private const string PauseControl = "Pause";

    public GlobalVariable GameRunning;
    public GlobalVariable SnowmanHealth;
    public Snowman Snowman;

    public GameObject StartScreen;
    public GameObject PauseScreen;

    public static GameController Instance;

    private Player _playerControls;

    private void Awake() {
        Instance = this;
        
        GameRunning.Value = 0;
        _playerControls = ReInput.players.GetPlayer(0);
    }

    // Start is called before the first frame update
    void Start() {
        SnowmanHealth.ReachedMinimum += GameOver;
        GameRunning.ReachedMaximum += StartGame;
    }

    private void OnDestroy() {
        SnowmanHealth.ReachedMinimum -= GameOver;
        GameRunning.ReachedMaximum -= StartGame;
    }

    private void StartGame() {
        // Cursor.visible = false;
        // Cursor.lockState = CursorLockMode.Locked;
    }

    private void GameOver() {
        GameRunning.Value = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        PlayerPrefs.SetInt(HasCompletedAtLeastOneGame, 1);
    }

    public void Update() {
        if (_playerControls.GetButtonDown(PauseControl)) {
            PauseUnpause();
            
            // Cursor.visible = paused;
            // Cursor.lockState = paused ? CursorLockMode.None : CursorLockMode.Locked;
        }
    }

    public void PauseUnpause() {
        GameRunning.Value = 1 - GameRunning.Value;
        bool paused = GameRunning.Value <= 0;
        Time.timeScale = GameRunning.Value;
        StartScreen.SetActive(false);
        PauseScreen.SetActive(paused);
    }
    
}
