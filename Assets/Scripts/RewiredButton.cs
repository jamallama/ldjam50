using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.UI;

public class RewiredButton : MonoBehaviour {
    public string ButtonName;
    private Player playerControls;

    // Start is called before the first frame update
    void Start()
    {
        playerControls = ReInput.players.GetPlayer(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (playerControls.GetButtonDown(ButtonName)) {
            GetComponent<Button>().onClick.Invoke();
        }
    }
}
