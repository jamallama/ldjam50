using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LoreSelector : MonoBehaviour {
    public TextAsset Lore;
    
    // Start is called before the first frame update
    void Start() {
        String[] entries = Lore.text.Split(new[] {"\r", "\n"}, StringSplitOptions.RemoveEmptyEntries);
        // If this is the first time playing, pick the first entry. Otherwise pick randomly. 
        string pickedLoreEntry = PlayerPrefs.GetInt(GameController.HasCompletedAtLeastOneGame, 0) == 0
                                ? entries[0] 
                                : entries[Random.Range(0, entries.Length)];
        GetComponent<TextMeshProUGUI>().text = pickedLoreEntry;
    }
}
