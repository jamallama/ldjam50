using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformExclusionRegionBox : PlatformExclusionRegion {

    private bool _boundsSet;
    private float _left;
    private float _right;
    private float _up;
    private float _down;

    private void SetBounds() {
        Transform t = transform;
        Vector3 tPos = t.position;
        Vector3 tSca = t.localScale;
        _left = tPos.x - tSca.x * 0.5f;
        _right = tPos.x + tSca.x * 0.5f;
        _up = tPos.z + tSca.z * 0.5f;
        _down = tPos.z - tSca.z * 0.5f;
        _boundsSet = true;
    }
    
    public override bool ExcludesPoint(Vector3 point) {
        if (!_boundsSet) SetBounds();
        if (point.x < _left - exclusionMargin) return false;
        if (point.x > _right + exclusionMargin) return false;
        if (point.z > _up + exclusionMargin) return false;
        if (point.z < _down - exclusionMargin) return false;
        return true;
    }
    
}
