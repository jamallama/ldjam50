using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class IcePlatformManager : MonoBehaviour {

    public static IcePlatformManager Instance;

    public GameObject IcePlatformPrefab;
    public float MaxDistance;
    public int MaxPlatformAttempts = 100;
    public float PlatformStartY;

    public float TimeBetweenSpawnsMin;
    public float TimeBetweenSpawnsMax;

    public int PlatformsToSpawn = 100;
    private float _waitUntilNextPlatform;

    public AnimationCurve AdditionalPlatformsOverTime;
    private float _newPlatformJuice;
    
    public AnimationCurve Concentration;

    private List<PlatformExclusionRegion> _exclusionRegions = new List<PlatformExclusionRegion>();

    private List<IcePlatformController> _activePlatforms = new List<IcePlatformController>();

    public void Awake() {
        Instance = this;
    }

    private void Update() {
        _waitUntilNextPlatform -= Time.deltaTime;
        if (PlatformsToSpawn > 0 && _waitUntilNextPlatform < 0f) {
            spawnNewIcePlatform();
        }

        _newPlatformJuice += Time.deltaTime * AdditionalPlatformsOverTime.Evaluate(Time.time);
        if (_newPlatformJuice > 1f) {
            _newPlatformJuice -= 1f;
            PlatformsToSpawn += 1;
        }
    }

    public void SpawnNewCatSpawnPlatform(GameObject catSpawnPrefab, float minDist = 0f, float maxDist = 1f) {
        DoSpawnNewPlatform(catSpawnPrefab, minDist, maxDist);
    }

    private void spawnNewIcePlatform(float minDist = 0f, float maxDist = 1f) {
        PlatformsToSpawn--;
        _waitUntilNextPlatform = Random.Range(TimeBetweenSpawnsMin, TimeBetweenSpawnsMax);
        DoSpawnNewPlatform(IcePlatformPrefab, minDist, maxDist);
    }

    private void DoSpawnNewPlatform(GameObject platformPrefab, float minDist = 0f, float maxDist = 1f) {
        if (!findEmptyPoint(out var spawnPoint, minDist, maxDist)) return;
        Transform t = Instantiate(platformPrefab, spawnPoint, Quaternion.identity, transform).transform;
        t.Rotate(0f, Random.Range(-10f, 10f), 0f);
        t.localScale *= Random.Range(0.9f, 1.1f);
    }

    private bool findEmptyPoint(out Vector3 point, float minDist = 0f, float maxDist = 1f) {
        for (int i = 0; i < MaxPlatformAttempts; i++) {
            Vector2 xz = Random.insideUnitCircle;
            float distance = minDist + xz.magnitude * (maxDist - minDist);
            xz = xz.normalized * Concentration.Evaluate(distance);
            Vector3 point3 = new Vector3(xz.x * MaxDistance, PlatformStartY, xz.y * MaxDistance);
            if (CanSpawnPlatformAtPoint(point3)) {
                point = point3;
                return true;
            }
        }
        point = Vector3.zero;
        return false;
    }
    
    private bool CanSpawnPlatformAtPoint(Vector3 point) {
        return _exclusionRegions.All(region => !region.ExcludesPoint(point));
    }

    public void RegisterExclusionRegion(PlatformExclusionRegion region) {
        _exclusionRegions.AddIfUnique(region);
    }
    
    public void UnregisterExclusionRegion (PlatformExclusionRegion region) {
        _exclusionRegions.Remove(region);
    }

    public void RegisterPlatform(IcePlatformController ipc) {
        _activePlatforms.AddIfUnique(ipc);
    }

    public void UnregisterPlatform(IcePlatformController ipc) {
        _activePlatforms.Remove(ipc);
    }

    public List<IcePlatformController> GetActivePlatforms() {
        return _activePlatforms;
    }
    
}
