using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformExclusionRegion : MonoBehaviour {

    public const float exclusionMargin = 1.5f;
    
    public virtual bool ExcludesPoint(Vector3 point) {
        return false;
    }

    private void Start() {
        if (IcePlatformManager.Instance == null) { Debug.LogError("IcePlatformManager.Instance == null"); return; }
        IcePlatformManager.Instance.RegisterExclusionRegion(this);
    }

    private void OnDestroy() {
        if (IcePlatformManager.Instance == null) return;
        IcePlatformManager.Instance.UnregisterExclusionRegion(this);
    }
    
}
