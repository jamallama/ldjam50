using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformExclusionRegionCircle : PlatformExclusionRegion {

    public float Radius = 1f;
    
    public override bool ExcludesPoint(Vector3 point) {
        Vector3 myPos = transform.position;
        Vector2 point2 = new Vector2(point.x, point.z);
        Vector2 myPoint2 = new Vector2(myPos.x, myPos.z);
        return Vector2.Distance(point2, myPoint2) < Radius + exclusionMargin;
    }
    
}
