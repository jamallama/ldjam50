using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcePlatformPenguinDetector : MonoBehaviour {

    public float ExtraGroundedDuration = 0.06f;
    private float _timeOfLastRegisteredPenguin;

    public bool HasPenguin() {
        return Time.time < _timeOfLastRegisteredPenguin + ExtraGroundedDuration;
    }
    
    public void RegisterPenguinGroundedHere() {
        _timeOfLastRegisteredPenguin = Time.time;
    }
    
}
