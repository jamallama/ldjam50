using System.Collections.Generic;
using System.Collections; // SFX - Added for delay SFX IEnumerator (Coroutine)
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class IcePlatformController : MonoBehaviour {

    [Header("Can Sink")]
    public bool CanSink = true;
    
    [Space]
    public AnimationCurve EntranceRiseCurve;
    public float RootYTarget = -0.77f;
    public float BobSize = 0.4f;
    public float BobSpeed = 0.1f;
    public float BobTimeOffsetPerX;
    public float BobTimeOffsetPerZ;
    
    private float _targetYCur = -0.77f;
    private float _targetYVel;
    public float smoothTime;
    private float _startY;
    
    private Transform _myTransform;
    private float _lifeTimer;

    public GameObject HealthyPlatform;
    public GameObject CrackedPlatform;
    public GameObject SinkingPlatform;
    public GameObject ColliderObject;
    public List<IcePlatformPenguinDetector> PenguinDetectors;
    public float GraceTimeBetweenBreakageSteps;
    private float _timeOfLastBreakage;
    private float _breakageStepsSoFar;
    private bool _hasHadLackOfPenguinSinceLastBreakageStep = true;

    public AnimationCurve SinkingCurve;
    public float SinkingTargetY;

    public bool IsSinking => _isSinking;
    private bool _isSinking = false;
    private float _targetYWhenSinkingStarted;
    private float _timeWhenSinkingStarted;

    public List<ParticleSystem> CrackParticles;

    public bool SpawnCats;
    public float CatSpawnFrequency;
    public float DelayBeforeSpawningFirstCat;
    public CatEnemy CatPrefab;
    public Transform CatSpawnLocation;

    public float DelayBeforeSubmergedEffect;
    public GameObject SubmergedEffectPrefab;
    private bool _hasDoneSubmergedEffect;

    private float _timeUntilNextCatSpawn;
    private bool _couldEverSpawnCats;

    private void Awake() {
        _myTransform = transform;
        _couldEverSpawnCats = SpawnCats;
    }

    private void Start() {
        _startY = _myTransform.position.y;
        _targetYCur = RootYTarget;
        _timeUntilNextCatSpawn = DelayBeforeSpawningFirstCat;
        _currentMeltHealthRemaining = StartingMeltHealth;
        _originalScaleFactor = transform.localScale.x;
        if (SpawnCats) {
            EnemyManager.Instance.RegisterSpawnerPlatform(this);
        }
        
        IcePlatformManager.Instance.RegisterPlatform(this);
    }

    private void Update() {
        _lifeTimer += Time.deltaTime;
        Vector3 oldPos = _myTransform.position;
        _myTransform.position = new Vector3(oldPos.x, Mathf.LerpUnclamped(_startY, _targetYCur, EntranceRiseCurve.Evaluate(_lifeTimer)), oldPos.z);

        if (_isSinking) {
            float sinkingTimeElapsed = Time.time - _timeWhenSinkingStarted;
            if (!_hasDoneSubmergedEffect && sinkingTimeElapsed > DelayBeforeSubmergedEffect) {
                _hasDoneSubmergedEffect = true;
                Instantiate(SubmergedEffectPrefab, new Vector3(oldPos.x, -0.35f, oldPos.z), Quaternion.identity);
            }
            
            float newTargetY = Mathf.LerpUnclamped(_targetYWhenSinkingStarted, SinkingTargetY, SinkingCurve.Evaluate(sinkingTimeElapsed));
            _targetYCur = Mathf.SmoothDamp(_targetYCur, newTargetY, ref _targetYVel, smoothTime);
            if (Time.time - _timeWhenSinkingStarted > SinkingCurve.Duration()) {
                Destroy(gameObject);
            }
        }
        
        else {
            float newTargetY = RootYTarget + Mathf.Sin( BobSpeed * Time.time + oldPos.x * BobTimeOffsetPerX + oldPos.z * BobTimeOffsetPerZ) * BobSize;
            _targetYCur = Mathf.SmoothDamp(_targetYCur, newTargetY, ref _targetYVel, smoothTime);
        }

        if (CanSink && PenguinDetectors.Any(e => e.HasPenguin()) && Time.time - _timeOfLastBreakage > GraceTimeBetweenBreakageSteps && _hasHadLackOfPenguinSinceLastBreakageStep) {
           DoBreakageStep();
        }

        if (!_hasHadLackOfPenguinSinceLastBreakageStep && !PenguinDetectors.Any(e => e.HasPenguin())) {
            _hasHadLackOfPenguinSinceLastBreakageStep = true;
        }

        if (SpawnCats) {
            TrySpawnCat();
        }

        if (platformIsMelting) {
            MeltPlatform(meltingSpeed * Time.deltaTime);
        }
    }

    private void TrySpawnCat() {
        _timeUntilNextCatSpawn -= Time.deltaTime;
        if (_timeUntilNextCatSpawn > 0) return;
        
        Instantiate(CatPrefab, CatSpawnLocation.position, Quaternion.identity, EnemyManager.Instance.SpawnBucket);
        _timeUntilNextCatSpawn = CatSpawnFrequency;
    }

    private void DoBreakageStep() {
        _timeOfLastBreakage = Time.time;
        _hasHadLackOfPenguinSinceLastBreakageStep = false;
        if (_breakageStepsSoFar == 0) {
            // SFX
            if (!_couldEverSpawnCats) {
                AudioController.Instance.PlatformCrackSFX(gameObject);
            } else {
                AudioController.Instance.PlatformCollapseSFX(gameObject);
            }

            SpawnCats = false;
            
            HealthyPlatform.SetActive(false);
            CrackedPlatform.SetActive(true);
            _breakageStepsSoFar++;
            CrackedPlatform.transform.Rotate(0f, Random.Range(0, 6) * 60f, 0f);
            foreach (ParticleSystem ps in CrackParticles) {
                ps.Play();
            }
            CameraController.Instance.ApplyCamshake(.25f);
        }
        else if (_breakageStepsSoFar == 1) {
            if (SinkingPlatform != null) {
                CrackedPlatform.SetActive(false);
                SinkingPlatform.SetActive(true);
            }
            foreach (ParticleSystem ps in CrackParticles) {
                ps.Play();
            }
            CameraController.Instance.ApplyCamshake(.25f);
            _breakageStepsSoFar++;
            Sink();
        }
    }

    // SFX
    private IEnumerator DelaySinkSFX() {
        yield return new WaitForSeconds(1.0f);
        AudioController.Instance.PlatformSinkSFX(gameObject);
    }

    private void Sink() {
        // SFX
        AudioController.Instance.SplashSFX(gameObject);
        StartCoroutine(DelaySinkSFX());

        _isSinking = true;
        _timeWhenSinkingStarted = Time.time;
        _targetYWhenSinkingStarted = _targetYCur;
            
        if (SpawnCats) {
            // Turn off cat spawning now that this platform is sinking
            SpawnCats = false;
            EnemyManager.Instance.UnRegisterSpawnerPlatform(this);
        }
    }


    public void OnCatIsClose(CatEnemy enemy) {
        platformIsMelting = true;
        meltingSpeed = Mathf.Max(meltingSpeed, enemy.Config.MeltStrength);
    }

    public void OnCatLeft() {
        platformIsMelting = false;
        meltingSpeed = 0;
    }

    [Header("Melting")] 
    public float StartingMeltHealth;
    private float _currentMeltHealthRemaining;
    private bool platformIsMelting;
    private float meltingSpeed;
    private float _originalScaleFactor;

    private void MeltPlatform(float damageAmount) {
        if (!CanSink) return;
        if (_isSinking) return;
        if (_couldEverSpawnCats) return;

        _currentMeltHealthRemaining -= damageAmount;
        Mathf.Clamp(_currentMeltHealthRemaining, 0, StartingMeltHealth);
        Transform t = transform;
        Vector3 scale = t.localScale;
        // Only let the platform get half as small as the original size
        float scaleSize = (_currentMeltHealthRemaining + (StartingMeltHealth - _currentMeltHealthRemaining) / 2) / StartingMeltHealth;
        scale.x = scaleSize * _originalScaleFactor;
        scale.z = scaleSize * _originalScaleFactor;
        t.localScale = scale;
        if (_currentMeltHealthRemaining <= 0) {
            Sink();
        }
    }

    private void OnDestroy() {
        if (IcePlatformManager.Instance != null) {
            IcePlatformManager.Instance.UnregisterPlatform(this);
        }
    }
}
