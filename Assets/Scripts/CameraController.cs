﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController Instance;
    private Transform playerTransform;
    public Camera Camera;

    public Vector3 DesiredOffset;
    public AnimationCurve ChaseTimeByDistanceFromDesiredPosition;
    private Vector3 DesiredPosition => playerTransform.position + (DesiredOffset * DesiredPositionMultiplierByTime.Evaluate(_elapsedTime));
    private float distanceFromDesiredPosition => Vector3.Distance(DesiredPosition, transform.position);
    private Vector3 velocity;

   public FXController CamshakeFX;

    public Gradient BackgroundColorGradient;
    public float BackgroundColorTime;
    private float backgroundColorTimer;
    
    // Following the player's aim
    private PlayerShootController _shootController;
    private Vector3 AimDir => _shootController.AimDirection;

    private Vector3 _aimOffsetCur;
    private Vector3 _aimOffsetVel;
    public float AimOffsetSmoothTime;
    public float AimOffsetDistanceMultiplier;
    
    
    public Gradient BackgroundColorGradientVictory;

    private float victoryBlendCur;
    private float victoryBlendVel;

    private float _elapsedTime;
    public AnimationCurve DesiredPositionMultiplierByTime;

    // [Header("Zoom Out")]
    // public AnimationCurve ZoomMultiplierDuringFirstCutscene;
    // public AnimationCurve ZoomMultiplierAfterFirstCutscene;
    // public float ZoomMultiplierEaseTime;
    // private float cutsceneTimer;
    // private float zoomMultiplierCur;
    // private float zoomMultiplierVel;
    // private float timeFirstCutsceneEnded;
    // private float zoomMultiplierAtEndOfCutscene;
    // private float ZoomMultiplierTargetDuringFirstCutscene => ZoomMultiplierDuringFirstCutscene.Evaluate(cutsceneTimer);
    // private float ZoomMultiplierTargetAfterFirstCutscene => Mathf.Lerp(zoomMultiplierAtEndOfCutscene, 1f, ZoomMultiplierAfterFirstCutscene.Evaluate(Time.time - timeFirstCutsceneEnded));
    // private bool wasInCutscene;

    [Header("Player Object")]
    public GameObject PlayerObject;
        
    private void Awake() {
        Instance = this;
    }

    private void Start() {
        playerTransform = PlayerObject.transform;
        _shootController = PlayerObject.GetComponent<PlayerShootController>();
        // zoomMultiplierCur = ZoomMultiplierDuringFirstCutscene.Evaluate(0);
        transform.position = DesiredPosition;
    }

    private void Update() {

        _elapsedTime += Time.deltaTime;
        
        transform.position = Vector3.SmoothDamp(transform.position, DesiredPosition, ref velocity, ChaseTimeByDistanceFromDesiredPosition.Evaluate(distanceFromDesiredPosition));
        
    }

    public void ApplyCamshake(float amount) {
      CamshakeFX.TriggerWithArgs(new FXArgs{Amplitude = amount});
    }
    
}
