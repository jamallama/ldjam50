using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameplaySceneUIAudio : MonoBehaviour {

    public void UIClickSFX() {
        AudioController.Instance.UIButtonClick();
    }
    public void UIButtonSelect() {
        AudioController.Instance.UIButtonClick();
    }

    public void StopMusic() {
        // Need a way to know if the player is transition from the pause menu to the main menu so we can stop the music immediately
        if (AudioController.Instance.currentMusicSource != null) { AudioController.Instance.currentMusicSource.Stop(); }
    }

}
