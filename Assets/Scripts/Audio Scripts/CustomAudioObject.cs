﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;



[CreateAssetMenu(menuName = "Audio Assets/Custom Audio Object")]
[RequireComponent (typeof (AudioSource))]
public class CustomAudioObject : ScriptableObject {
	public enum Channel { Master, UI, AMB, MX, SFX, DIA };
	[Header("Custom Data")]
	public Channel channel; // Anything on the MX channel will swap dyanamically between the two MX sub channel (MX_One, MX_Two)
	public AudioClip[] audioClips;
	// Only needed for music loops
	// Calculate a Clip’s exact duration
	// double duration = (double)AudioClip.samples / AudioClip.frequency;
	// loop length in samples / audio clip sample rate
	public double lengthInSamples;
	public double sampleRate;

	[Header("Audio Source Settings")]
	[Range(0.0f, 1.0f)]
	public float spatialBlend = 0.0f;
	[Range(0.0f, 1.0f)]
	public float volume = 1.0f;
	public Vector2 randVolRange = Vector2.zero;
	public float pitch = 1.0f;
	public Vector2 randPitchRange = Vector2.zero;
	public bool loop = false;

	public double LoopDuration() {
		return lengthInSamples / sampleRate;
	}
}
