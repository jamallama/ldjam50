﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;



public class AudioController : MonoBehaviour {
	// Singleton pattern getter and instance variable;
	private static AudioController instance;
	public static AudioController Instance { get { return instance; } }

	public AudioMixer mixer;
	public AudioMixerGroup[] channels;
	public AudioMixerGroup[] musicSubChannels;

	public AudioSource ambientAudioSource;

	public CustomAudioObject[] musicLoopData;
	public CustomAudioObject[] uiSFX;
	public CustomAudioObject[] SFX;

	[HideInInspector] public int musicSubChannelIndex = 0;
	public double musicStartDelay = 0.2;
	public double dynamicMusicLookAhead = 1.000d;
	// [HideInInspector]
	public double scheduledMusicTime = 0.000d;
	// Abstracted value used to determine what dynamic music loop should be played (replaced 'danger level')
	[HideInInspector] public int previousMusicLoopIndex = 0;
	// [HideInInspector] public int musicLoopIndex = 0;

	[HideInInspector] public AudioSource currentMusicSource;


	private int mxGroupIndex = 0;
	private int mxLoopIndex = -1;
	
	

	[HideInInspector] public GameObject loopingSFXObject;
	[HideInInspector] public AudioSource loopingSFXSource;

	[Header("Data Object References")]
	public GlobalVariable snowmanHealth;
	public GlobalVariable gameRunning;
	public GlobalVariable collectedSnow;
	public float potentialEndHealthPercentThreshold = 0.25f;
	[Range(0, 100)]
	public float percentAddedPerEnemey = 2;
	public bool musicTimerStarted = false;
	public bool startMusic = false;
	public bool gameStarted = false;
	// public bool endRunMusicEngaged = false;
	public string audioGameState;
	public string previousAudioGameState;
	public string subState;
	public string previousSubState;
	public float gameRunningValue;
	public float previousGameRunningValue;

	private Scene currentScene;
	private Scene previousScene;
	[HideInInspector] public int numEnemies = 0;
	private float calculatedThreshold = 0.0f;
	// private Scene previousScene;
	// private AudioListener listener;



	public void Awake() {
		DontDestroyOnLoad(gameObject);
		if (instance != null && instance != this) {
			Destroy(gameObject);
		} else {
			instance = this;
		}
	}



	public void Start() {
		// previousScene = currentScene;
		// ResetClipController();
		StartCoroutine(FadeInAMBSource(2.0f));
		// StartCoroutine(DelayMusicStart());
	}

	
	public IEnumerator DelayMusicStart() {
		float progress = 0.0f;
		while (progress <= musicStartDelay) {
			progress += Time.deltaTime * Time.timeScale;
			yield return null;
		}
		scheduledMusicTime = 0.000d;
		scheduledMusicTime = AudioSettings.dspTime + scheduledMusicTime;
		startMusic = true;
	}

	public IEnumerator FadeInAMBSource(float duration) {
		yield return new WaitForSeconds(0.25f); // Waits to ensure scene is done loading.
		float progress = 0.0f;
		AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
		while (progress <= duration) {
			progress += Time.deltaTime * Time.timeScale;
			float percent = Mathf.Clamp01(progress / duration);
			float curvePercent = curve.Evaluate(percent);
			ambientAudioSource.volume = Mathf.Lerp(0.0f, 1.0f, curvePercent);
			yield return null;
		}
	}


	// Update function is used to check danger level at a given point just before a music loop is ready to end
	// to determine what music should be scheduled to play next.
	public void Update() {


		DetermineAudioState();

		
		if (startMusic && AudioSettings.dspTime > scheduledMusicTime - dynamicMusicLookAhead) {

			WrapMusicLoops();

			DetermineMusicLoop(mxGroupIndex, mxLoopIndex);
			if (audioGameState == "Gameplay Ended") {
				subState = "Silence";
				if (SubStateChanged() && subState == "Silence") {
					mxLoopIndex = -1;
					FadeChannel("SFX_Vol", 3.0f, -20.0f);
				}
			}
		}

		previousScene = currentScene;
		previousAudioGameState = audioGameState;
		previousSubState = subState;
		previousGameRunningValue = gameRunningValue;
	}


	
	private void DetermineAudioState(bool logState = false) {

		// Calculates the snowman's health as a percentage of its maximum
		float snowmanHealthPercent = snowmanHealth.Value / snowmanHealth.MaxValue;
		// Raises the potential end game snowman health percent threshold by 0.75% per enemy.
		calculatedThreshold = potentialEndHealthPercentThreshold + numEnemies * (percentAddedPerEnemey * 0.0075f);
		calculatedThreshold = Mathf.Clamp(calculatedThreshold, potentialEndHealthPercentThreshold, 0.5f);
		// Pauses the audio listener if the game is paused.
		AudioListener.pause = Time.timeScale < 1.0;
		// Defines the currentScene as whatever the active scene is
		currentScene = SceneManager.GetActiveScene();
		// Defines my local copy of the gameRunningValue by whatever the global gameRunning.Value is
		gameRunningValue = gameRunning.Value;


		if (audioGameState == "Gameplay Started") {
			// To look for the change from the 'Body' substate to the 'Crescendo' substate, we need to check to see if the Body loops have reach the end of the group.
			// If we've reach the last loop of the 'Body' subState, we need to switch to the Crescendo loops.
			if (subState == "Body" && AtEndOfGroup()) {
				subState = "Crescendo";
			}
			if (snowmanHealthPercent < calculatedThreshold && subState != "Outro") {
				subState = "Outro";
			} else if (snowmanHealthPercent >= calculatedThreshold && previousSubState == "Outro" && subState != "Crescendo") {
				subState = "Crescendo";
			}
		}



		// A new scene was loaded on this frame: executes once on change.
		if (LoadedNewScene()) {
			// Debug.Log("Loaded new scene on this frame: " + currentScene.name);
			if (currentScene.name == "Menu") {
				startMusic = false;
				StartCoroutine(DelayMusicStart());
				numEnemies = 0;
				audioGameState = "Menu";
				subState = "Menu";
				// Fades the SFX volume back up to 0.0 after post-game fade out on Gameplay scene when returning to Menu scene.
				// On menu load, this will still run, but should have no effect since the starting and target volumes are the same.
				FadeChannel("SFX_Vol", 1.0f, 0.0f);
			}
			if (currentScene.name == "Gameplay") {
				numEnemies = 0;
				audioGameState = "Gameplay Not Started";
				subState = "Intro";
			}
		}

		// The audio state changed: executed once on change.
		if (AudioGameStateChanged()) {
			// Debug.Log("The audio game state has changed from " + previousAudioGameState + " to " + audioGameState);
			
			// Determine subStates here for main gameplay music
			if (audioGameState == "Gameplay Started") {
				subState = "Body";
				// Fades the SFX volume back up to 0.0 after post-game fade out.
				// On new games, this will still run, but have no effect since the starting and target volumes are the same.
				FadeChannel("SFX_Vol", 1.0f, 0.0f);
			}
			if (audioGameState == "Gameplay Ended" && subState != "End" && subState != "Silence") {
				subState = "End";
			}
		}

		// The gameRunning value changed: executed once on change.
		if (GameRunningStateChanged()) {
			// Debug.Log("The game running state has changed from " + previousGameRunningValue + " to " + gameRunningValue);
			if (audioGameState == "Gameplay Not Started" && gameRunningValue == 1) {
				audioGameState = "Gameplay Started";
				// Recursive call which we need to in-turn call the AudioGameStateChanged() function so
				// the music group will change to the 'Body' group instead of staying on the Intro group.
				DetermineAudioState();
			} else if (audioGameState == "Gameplay Started" && snowmanHealthPercent <= 0.0f && gameRunningValue == 0) {
				audioGameState = "Gameplay Ended";
				// Recursive call which we need to in-turn call the AudioGameStateChanged() function so
				// the music group will change to the 'End' group instead of staying on the Outro or Crescendo group.
				DetermineAudioState();
			}
		}

		if (subState != "Silence") {
			DetermineMusicBySubState();
		} else {
			numEnemies = 0;
			mxGroupIndex = 6;
		}
		
		if (logState) { Debug.Log(audioGameState); }
	}

	private void DetermineMusicBySubState() {
		// The sub state changed: executed once on change.
		if (SubStateChanged()) {
			// Debug.Log("The sub state has changed from " + previousSubState + " to " + subState);
			if (subState == "Menu") {
				mxLoopIndex = mxGroupIndex != 0 ? -1 : mxLoopIndex; // <-- For whatever reason this isn;'t doing what I want, but it's fine for now.
				mxGroupIndex = 0;
			}
			if (subState == "Intro") {
				if (previousSubState == "Silence" || previousSubState == "End") {
					scheduledMusicTime = 0.000d;
					scheduledMusicTime = AudioSettings.dspTime + scheduledMusicTime;
				}
				mxGroupIndex = 1;
				mxLoopIndex = -1;
			}
			if (subState == "Body") {
				// Since the Intro loops are included in the body group, we only want to reset
				// to the beggining if the current loops is the second one in the Intro group.
				mxLoopIndex = mxLoopIndex == 1 ? -1 : mxLoopIndex;
				mxGroupIndex = 2;
			}
			if (subState == "Crescendo") {
				mxGroupIndex = 3;
				mxLoopIndex = -1;
			}
			if (subState == "Outro") {
				mxGroupIndex = 4;
				mxLoopIndex = -1;
			}
			if (subState == "End") {
				numEnemies = 0;
				mxGroupIndex = 5;
			}
		}
	}



	private bool LoadedNewScene() {
		return currentScene.name != previousScene.name;
	}
	private bool AudioGameStateChanged() {
		return audioGameState != previousAudioGameState;
	}
	private bool SubStateChanged() {
		return subState != previousSubState;
	}
	private bool GameRunningStateChanged() {
		return gameRunningValue != previousGameRunningValue;
	}
	private bool AtEndOfGroup() {
		return mxLoopIndex + 1 >= musicLoopData[mxGroupIndex].audioClips.Length;
	}



	private void WrapMusicLoops() {
		if (AtEndOfGroup()) {
			mxLoopIndex = 0;
		} else {
			mxLoopIndex++;
		}
	}
	private float CollectedSnowPercent() {
		return collectedSnow.Value / collectedSnow.MaxValue;
	}

	// Helper functions to keep calls to specific audio sources in one place if updates needed
	// Calls use an index of the different types of sound fx supplied as arrays in the inspector.
	// To access from another script, call AudioController.Instance.SFXSoundEffectGroupOne(), or whatever other helper function you want to use.

	public void TitleHit() { CreateSFXSource(2); }

	// SFX
	// If the sound effect is coming from something that moves, or is in another location, then the soundOriginObject should be whatever that game object is
	// and the SFXSoundEffectGroupXXX will make the created sound object a child of that object, giving the audio listener (camera) spatial awareness of the sounds position.
	public void JumpSFX(GameObject soundOriginObject = null) { CreateSFXSource(0, soundOriginObject); }
	public void DoubleJumpSFX(GameObject soundOriginObject = null) { CreateSFXSource(1, soundOriginObject); }
	public void PlatformCrackSFX(GameObject soundOriginObject = null) { CreateSFXSource(2, soundOriginObject); }
	public void ThrowSnowballSFX(GameObject soundOriginObject = null) { CreateSFXSource(3, soundOriginObject); }
	public void SplashSFX(GameObject soundOriginObject = null) { CreateSFXSource(4, soundOriginObject); }
	public void SnowballImpactSFX(GameObject soundOriginObject = null) { CreateSFXSource(5, soundOriginObject); }
	public void BigCatExplosionSFX(GameObject soundOriginObject = null) { CreateSFXSource(6, soundOriginObject); }
	public void SmallCatExplosionSFX(GameObject soundOriginObject = null) { CreateSFXSource(7, soundOriginObject); }
	public void SnowPickupSFX(GameObject soundOriginObject = null) { CreateSFXSource(8, soundOriginObject); }
	public void PlatformCollapseSFX(GameObject soundOriginObject = null) { CreateSFXSource(9, soundOriginObject); }
	public void PlatformSinkSFX(GameObject soundOriginObject = null) { CreateSFXSource(10, soundOriginObject); }
	public void PenguinLandSFX(GameObject soundOriginObject = null) { CreateSFXSource(11, soundOriginObject); }
	public void SqueakyToySFX(GameObject soundOriginObject = null) { CreateSFXSource(15, soundOriginObject); }
	// Title Stabs are special case SFX which are thematically DIRECTLY tied to the music, but not actual music tracks.
	public void TitleStabThe(GameObject soundOriginObject = null) { CreateSFXSource(12, soundOriginObject); }
	public void TitleStabInevitable(GameObject soundOriginObject = null) { CreateSFXSource(13, soundOriginObject); }
	public void TitleStabSnowman(GameObject soundOriginObject = null) { CreateSFXSource(14, soundOriginObject); }
	public void OutOfSnow(GameObject soundOriginObject = null) { CreateSFXSource(16, soundOriginObject); }


	// UI SFX
	public void UIButtonClick() { CreateUISFXSource(0); }
	public void UIHatClick() { CreateUISFXSource(1); }
	public void UIButtonSelect() { CreateUISFXSource(2); }


	public void DetermineMusicLoop(int groupIndex, int loopIndex) {
		CreateMusicSource(groupIndex, loopIndex);
	}

	public void CreateMusicSource(int groupIndex, int loopIndex) {
		CreateAudioSource(musicLoopData, true, groupIndex, loopIndex);
	}

	public void CreateSFXSource(int index, GameObject soundOriginObject = null, bool setAsChild = false) {
		if (index == 8) {
			SFX[index].pitch = 1.0f + CollectedSnowPercent() * 0.17f;
		}
		CreateAudioSource(SFX, true, index, 0, soundOriginObject, setAsChild);
	}

	public void CreateUISFXSource(int index) {
		CreateAudioSource(uiSFX, true, index);
	}


	
	// Creates an audio source using a clip from the given array at index "i"
	// For music, then should be called once, when the state has changed
	// ***** We will probably need build in a cancellation check if something changes while the next music source is already queued. *****
	private void CreateAudioSource(CustomAudioObject[] audioArray, bool dontDestroyOnLoad, int groupIndex, int loopIndex = 0, GameObject soundOriginObject = null, bool setAsChild = false) {
		GameObject sourceObj = new GameObject("Audio Source");
		AudioSource source = sourceObj.AddComponent<AudioSource>();
		int channelIndex = GetChannelIndex(audioArray[groupIndex].channel);

		// Set all other needed parameters for the audio source


		// Non-music clips
		if (channelIndex != 3) {

			// SFX Specifically
			if (channelIndex == 6) {
				if (soundOriginObject != null) {
					sourceObj.transform.position = soundOriginObject.transform.position;
					if (setAsChild) {
						sourceObj.transform.parent = soundOriginObject.transform;
					}
				}
			}

			// Non-music audio source
			source.clip = audioArray[groupIndex].audioClips[Random.Range(0, audioArray[groupIndex].audioClips.Length)];
			source.spatialBlend = audioArray[groupIndex].spatialBlend;

			source.pitch = audioArray[groupIndex].pitch + Random.Range(audioArray[groupIndex].randPitchRange.x, audioArray[groupIndex].randPitchRange.y);
			source.volume = audioArray[groupIndex].volume + Random.Range(audioArray[groupIndex].randVolRange.x, audioArray[groupIndex].randVolRange.y);
			source.loop = audioArray[groupIndex].loop; // Loops if setting on custom object indicates to do so.
			source.outputAudioMixerGroup = channels[channelIndex];
			source.Play();

			if (audioArray[groupIndex].loop) {
				loopingSFXObject = sourceObj;
				loopingSFXSource = source;
			}
		} else {

			source.clip = audioArray[groupIndex].audioClips[loopIndex];

			// Toggle music sub channel for automatic assignment to the next channel if we need to perform fade-in, fade-out transitions.
			// This should only be performed when the source is a dynamic music source and in no other cases.
			source.outputAudioMixerGroup = musicSubChannels[musicSubChannelIndex];
			source.volume = audioArray[groupIndex].volume + Random.Range(audioArray[groupIndex].randVolRange.x, audioArray[groupIndex].randVolRange.y);
			musicSubChannelIndex = 1 - musicSubChannelIndex;

			// scheduledMusicTime will be whatever is set in the inspector by default on the first
			// call to this function to play music.
			// AudioSettings.dspTime is the current dspTime
			source.PlayScheduled(scheduledMusicTime);
			currentMusicSource = source;
			// Used for determining how close we are to needing to check the game state/ danger level and playing the next music loop

			// If this not object is not supposed to loop then set it's destruction timer for the length of the clip
			if (!audioArray[groupIndex].loop)
				scheduledMusicTime += audioArray[mxGroupIndex].LoopDuration();
		}

		// This should prevent menu music loop from being destroyed during scene transition
		// and the game should automatically start using the main game loops once the most recent
		// menu loop is finished.
		if (dontDestroyOnLoad) { DontDestroyOnLoad(sourceObj); }

		// Audio source object destroys itself once the clip is finished playing
		if (!audioArray[groupIndex].loop)
			Destroy(sourceObj, source.clip.length);
	}
		


	// Needed for the CreateAudioSource script to determine what channel a created source should play on for the mixer.
	public int GetChannelIndex(CustomAudioObject.Channel channel) {
		// public enum Channel { Master, UI, AMB, MX, SFX, DIA };
		// Master = 0
		// UI = 1
		// AMB = 2
		// MX = 3
		// SFX = 4
		// DIA = 5
		switch (channel) {
			case CustomAudioObject.Channel.DIA: return 5;
			case CustomAudioObject.Channel.SFX: return 4;
			case CustomAudioObject.Channel.MX: return 3;
			case CustomAudioObject.Channel.AMB: return 2;
			case CustomAudioObject.Channel.UI: return 1;
			case CustomAudioObject.Channel.Master: return 0;
		}
		return 0;
	}


	// Fade SFX Channel
	private IEnumerator Fade(string parameterName, float duration, float targetVol) {
		float progress = 0.0f;
		AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
		mixer.GetFloat(parameterName, out float startVol);
		while (progress <= duration) {
			progress += Time.deltaTime;
			float percent = Mathf.Clamp01(progress / duration);
			float curvePercent = curve.Evaluate(percent);
			mixer.SetFloat(parameterName, Mathf.Lerp(startVol, targetVol, curvePercent));
			yield return null;
		}
	}
	public void FadeChannel(string parameterName, float duration, float targetVol) {
		StartCoroutine(Fade(parameterName, duration, targetVol));
		// Debug.Log("Called fade channel.");
	}




		/*
		private void InterruptMusicNow(int clipIndex) {
			if (currentMusicSource != null) { currentMusicSource.Stop(); }
			GameObject sourceObj = new GameObject("Audio Source");
			AudioSource source = sourceObj.AddComponent<AudioSource>();
			source.clip = musicLoopData[7].audioClips[clipIndex];
			source.outputAudioMixerGroup = musicSubChannels[musicSubChannelIndex];
			musicSubChannelIndex = 1 - musicSubChannelIndex;
			source.Play();
			Destroy(sourceObj, source.clip.length);
		}
		*/


		/*

		private bool MainMenuLoaded() {
			return SceneManager.GetActiveScene().name == "Main Menu";
		}
		private void ResetClipController() {
			fadeInMenuMusic = true;
			gameEnd = false;
			scheduledMusicTime = 0.000d;
			scheduledMusicTime = AudioSettings.dspTime + scheduledMusicTime;
		}

		// Test function will change or loop music depending on danger level (or something).
		public void CheckDangerLevel(float currentDangerLevel) {
			// Does the transition crash from high to nothing and allows for a bit of a stress relief if the previous danger level was great than 0.66f
			// and the current one is less than or eual to 0.33f
			if (previousDangerLevel >= 0.666f &&
				currentDangerLevel <= 0.333f) {
				CreateMusicSource(11);
			} else {
				switch (dangerLevel) {
					case float dangerLevel when (dangerLevel <= 0.333f): CreateMusicSource(Random.Range(2, 4)); break; // Low to no danger
					case float dangerLevel when (dangerLevel > 0.333f && dangerLevel <= 0.666f): CreateMusicSource(Random.Range(4, 8)); break; // Midling to average danger
					case float dangerLevel when (dangerLevel > 0.666f): CreateMusicSource(Random.Range(8, 11)); break; // High Danger
				}
			}
			previousDangerLevel = dangerLevel;
		}


		public void LoopMenuMusic() {
			CreateAudioSource(mainMenuLoops, 0, true);
		}


		public IEnumerator FadeAlarm(float targetVol) {
			if (targetVol != 0.0f) {
				alarmFiring = true;
				alarm.Play();
			}
			float progress = 0.0f;
			AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
			float startVol = alarm.volume;
			while (progress <= alarmFadeDuration) {
				progress += Time.deltaTime * Time.timeScale;
				float percent = Mathf.Clamp01(progress / alarmFadeDuration);
				float curvePercent = curve.Evaluate(percent);
				alarm.volume = Mathf.Lerp(startVol, targetVol, curvePercent);
				yield return null;
			}
			if (targetVol == 0.0f) {
				alarmFiring = false;
				alarm.Stop();
			}
		}


		public void FadeInMX(float duration) {
			StopAllCoroutines();
			StartCoroutine(FadeInMXChannel(duration));
		}
		private IEnumerator FadeInMXChannel(float duration) {
			float progress = 0.0f;
			AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
			float startVol = -80.0f;
			float targetVol = 0.0f;
			while (progress <= duration) {
				progress += Time.deltaTime * Time.timeScale;
				float percent = Mathf.Clamp01(progress / duration);
				float curvePercent = curve.Evaluate(percent);
				mixer.SetFloat("MX_Vol", Mathf.Lerp(startVol, targetVol, curvePercent));
				yield return null;
			}
		}


		public void FadeOutMX(float duration) {
			StopAllCoroutines();
			StartCoroutine(FadeOutMXChannel(duration));
		}
		private IEnumerator FadeOutMXChannel(float duration) {
			float progress = 0.0f;
			AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
			float startVol = 0.0f;
			float targetVol = -80.0f;
			while (progress <= duration) {
				progress += Time.deltaTime * Time.timeScale;
				float percent = Mathf.Clamp01(progress / duration);
				float curvePercent = curve.Evaluate(percent);
				mixer.SetFloat("MX_Vol", Mathf.Lerp(startVol, targetVol, curvePercent));
				yield return null;
			}
		}


		public IEnumerator FadeSource(AudioSource source, float targetVol, float duration) {
			if (targetVol > 0.0f && !source.isPlaying) { source.Play(); }
			float progress = 0.0f;
			AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
			float startVol = source.volume;
			while (progress <= duration) {
				progress += Time.deltaTime * Time.timeScale;
				float percent = Mathf.Clamp01(progress / duration);
				float curvePercent = curve.Evaluate(percent);
				source.volume = Mathf.Lerp(startVol, targetVol, curvePercent);
				yield return null;
			}
			if (targetVol == 0.0f) { source.Stop(); }
		}

		public void FadeOutRomanceMusic() {
			StartCoroutine(FadeSource(winMusicSource, 0.0f, 2.0f));
		}
		*/
	}
