using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HatCollectedDisplay : MonoBehaviour {
    public HatGenerator HatGenerator;
    public TextMeshProUGUI Title;
    public Image Image;
    public GameObject gotIt;
    public GameObject missedIt;

    private void OnEnable() {
        var hat = HatGenerator.ToGenerate;
        if (hat == null) {
            gameObject.SetActive(false);
            return;
        }
        
        Title.text = hat.Name;
        Image.sprite = hat.Sprite;
        bool owned = HatGenerator.HatInventory.Hats.Contains(hat);
        gotIt.SetActive(owned);
        missedIt.SetActive(!owned);
    }
}
