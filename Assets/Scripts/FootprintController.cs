using System;
using UnityEngine;

public class FootprintController : MonoBehaviour {
    public ParticleSystem FootprintGenerator;
    public PlayerController PlayerController;
    private bool _wasGrounded;

    private void Awake() {
        FootprintGenerator.gameObject.SetActive(false);
        Invoke(nameof(TurnOnFootprints), .75f);
        if (name == "asdfasdfasdf") {   //never true. Just including this to ensure method is not stripped.
            TurnOnFootprints();
        }
    }

    private void TurnOnFootprints() {
        FootprintGenerator.gameObject.SetActive(true);
        FootprintGenerator.Clear();
    }

    private void Update() {
        if (PlayerController.IsGrounded != _wasGrounded) {
            _wasGrounded = PlayerController.IsGrounded;
            var emission = FootprintGenerator.emission;
            emission.enabled = _wasGrounded;
        }
    }
}
