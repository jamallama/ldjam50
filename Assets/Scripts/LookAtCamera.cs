
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    Transform target;
    void Start() {
        target = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
    }
}
