using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButtonController : MonoBehaviour {

    public AnimationCurve IntroCurve;
    public float TimeOfIntro;

    public AnimationCurve ClickedCurve;
    private float _timeOfLastClick = -20f;

    public AnimationCurve HighlightedCurveByTime;
    public float HighlightedScale;
    public float NotHighlightedScale;
    private float _highlightScaleCur;
    private float _highlightScaleVel;
    public float HighlightSmoothTime;
    private float _timeOfLastHighlighted;
    
    private float HighlightScaleTarget => !_isUsingGamepad ? HighlightedScale : _isSelected ? HighlightedScale * HighlightedCurveByTime.Evaluate(Time.time - _timeOfLastHighlighted) : NotHighlightedScale;

    public Image NotHighlightedDarkenImage;
    private float _highlightDarkeningCur;
    private float _highlightDarkeningVel;
    
    private float HighlightDarkeningTarget => !_isUsingGamepad ? 0f : _isSelected ? 0f : 1f;
    
    
    public bool _isSelected;
    private bool _isUsingGamepad;

    public Button Button;


    public void SetGamepad(bool usingGamepad) {
        _isUsingGamepad = usingGamepad;
    }

    public void SetSelected(bool isSelected, bool doSound = true) {
        _isSelected = isSelected;
        _timeOfLastHighlighted = Time.time;

        if (isSelected && doSound) {
            AudioController.Instance.UIButtonSelect();
        }
    }

    public void DoClick() {
        _timeOfLastClick = Time.time;
    }
    
    private void Update() {
        _highlightScaleCur = Mathf.SmoothDamp(_highlightScaleCur, HighlightScaleTarget, ref _highlightScaleVel, HighlightSmoothTime);
        _highlightDarkeningCur = Mathf.SmoothDamp(_highlightDarkeningCur, HighlightDarkeningTarget, ref _highlightDarkeningVel, HighlightSmoothTime);

        float clickScale = ClickedCurve.Evaluate(Time.time - _timeOfLastClick);
        float introScale = IntroCurve.Evaluate(Time.time - TimeOfIntro);

        transform.localScale = _highlightScaleCur * clickScale * introScale * Vector3.one;
        NotHighlightedDarkenImage.color = new Color(NotHighlightedDarkenImage.color.r, NotHighlightedDarkenImage.color.g, NotHighlightedDarkenImage.color.b, _highlightDarkeningCur);
    }

}
