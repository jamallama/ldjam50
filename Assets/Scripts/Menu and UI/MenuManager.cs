using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public static MenuManager Instance;
    
    [Header("Background Positions")]
    public RectTransform BackgroundRT;
    public float BackgroundPositionCustomize;
    public float BackgroundPositionMain;
    public float BackgroundPositionLeaderboard;

    private float _backgroundPositionStart;
    private float _backgroundPositionEnd;
    private float _backgroundPositionTarget;
    private float _backgroundPositionCur;
    private float _backgroundPositionVel;
    private float _timeOfLastBackgroundPositionChange = -20f;

    public AnimationCurve TransitionCurve;
    public float SmoothTime;

    private float _timeElapsedThisMenu;

    [Header("Intro Scheduling")]
    public float TimeOfWhiteIntro;
    public WhiteCoverController WhiteCoverController;
    private bool _hasDoneWhiteIntro;
    public float TimeOfTextThe;
    public GameObject TextObjectThe;
    private bool _hasDoneTextThe;
    public float TimeOfTextInevitable;
    public GameObject TextObjectInevitable;
    private bool _hasDoneTextInevitable;
    public float TimeOfTextSnowman;
    public GameObject TextObjectSnowman;
    private bool _hasDoneTextSnowman;
    public float TimeOfSheen;
    public GameObject SheenObject;
    private bool _hasDoneSheen;
    public AnimationCurve LogoHolderAnimationSpeedByTime;
    public Animator LogoHolderAnimator;

    public Animator PenguinDropAnimator;
    public float PenguinDropThreshold;
    private bool _hasDroppedPenguin;


    [Header("Buttons")]
    public List<MenuButtonController> Buttons;
    private Player _playerControls;
    private bool _usingGamepad = true;
    public float ButtonSelectCooldown;
    private float _timeOfLastSelectionChange;
    private const string MoveHorizontal = "MoveHorizontal";
    private const string MoveVertical = "MoveVertical";
    private const string Confirm = "Jump";
    public int CurrentButtonSelection = 0;
    private int _stashedButtonSelection;

    private List<MenuButtonController> _hatButtons = new List<MenuButtonController>();
    public int CurrentHatButtonSelection = -1;

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        _playerControls = ReInput.players.GetPlayer(0);
        _timeElapsedThisMenu = 0f;
        _backgroundPositionStart = _backgroundPositionEnd = _backgroundPositionTarget = _backgroundPositionCur = BackgroundPositionMain;
        Buttons[0].SetSelected(true, false);
    }
    
    private void Update() {

        _timeElapsedThisMenu += Time.deltaTime;
        if (!_hasDoneWhiteIntro && _timeElapsedThisMenu > TimeOfWhiteIntro) {WhiteCoverController.DoMenuIntro(); _hasDoneWhiteIntro = true;}
        if (!_hasDoneTextThe && _timeElapsedThisMenu > TimeOfTextThe) {TextObjectThe.SetActive(true); _hasDoneTextThe = true;}
        if (!_hasDoneTextInevitable && _timeElapsedThisMenu > TimeOfTextInevitable) {TextObjectInevitable.SetActive(true); _hasDoneTextInevitable = true;}

        if (!_hasDoneTextSnowman && _timeElapsedThisMenu > TimeOfTextSnowman) {
            TextObjectSnowman.SetActive(true); _hasDoneTextSnowman = true;
            Buttons[0].TimeOfIntro = Time.time + 1.25f;
            Buttons[1].TimeOfIntro = Time.time + 1.55f;
            Buttons[2].TimeOfIntro = Time.time + 1.4f;
            Buttons[3].TimeOfIntro = Time.time + 1.7f;
        }
        if (!_hasDoneSheen && _timeElapsedThisMenu > TimeOfSheen) {SheenObject.SetActive(true); _hasDoneSheen = true;}

        if (!_hasDroppedPenguin && _backgroundPositionCur > PenguinDropThreshold) {
            PenguinDropAnimator.SetBool("Visible", true);
            _hasDroppedPenguin = true;
        } else if (_hasDroppedPenguin && _backgroundPositionEnd <= PenguinDropThreshold) {
            PenguinDropAnimator.SetBool("Visible", false);
            _hasDroppedPenguin = false;
        }
        
        LogoHolderAnimator.speed = LogoHolderAnimationSpeedByTime.Evaluate(_timeElapsedThisMenu);
        
        _backgroundPositionTarget = Mathf.LerpUnclamped(_backgroundPositionStart, _backgroundPositionEnd, TransitionCurve.Evaluate(Time.time - _timeOfLastBackgroundPositionChange));
        _backgroundPositionCur = Mathf.SmoothDamp(_backgroundPositionCur, _backgroundPositionTarget, ref _backgroundPositionVel, SmoothTime);
        BackgroundRT.anchoredPosition = new Vector2(_backgroundPositionCur, BackgroundRT.anchoredPosition.y);

        UpdateButtons();
    }

    private void UpdateButtons() {
        if (_playerControls.controllers.GetLastActiveController().type == ControllerType.Mouse) {
            _usingGamepad = false;
        } else if (_playerControls.controllers.GetLastActiveController().type == ControllerType.Joystick) {
            _usingGamepad = true;
        }
        
        foreach (MenuButtonController mbc in Buttons) {
            mbc.SetGamepad(_usingGamepad);
        }
        
        foreach (MenuButtonController mbc in _hatButtons) {
            mbc.SetGamepad(_usingGamepad);
        }

        if (_playerControls.GetButtonDown(Confirm)) {
            if (CurrentHatButtonSelection == -1) {
                Buttons[CurrentButtonSelection].Button.onClick.Invoke();
            }
            else {
                _hatButtons[CurrentHatButtonSelection].Button.onClick.Invoke();
            }
        }

        if (!_usingGamepad) return;

        float horizontalInput = _playerControls.GetAxis(MoveHorizontal);
        float verticalInput = _playerControls.GetAxis(MoveVertical);
        if (Mathf.Abs(horizontalInput) < 0.25f && Mathf.Abs(verticalInput) < 0.25f) {
            _timeOfLastSelectionChange = -20f;
        } else if (horizontalInput < -0.25f && Mathf.Abs(horizontalInput) > Mathf.Abs(verticalInput) && Time.time - _timeOfLastSelectionChange > ButtonSelectCooldown) {
            MoveSelectionLeft();
        } else if (horizontalInput > 0.25f && Mathf.Abs(horizontalInput) > Mathf.Abs(verticalInput) && Time.time - _timeOfLastSelectionChange > ButtonSelectCooldown) {
            MoveSelectionRight();
        } else if (_hasDroppedPenguin && verticalInput > 0.25f && Mathf.Abs(verticalInput) > Mathf.Abs(horizontalInput) && Time.time - _timeOfLastSelectionChange > ButtonSelectCooldown) {
            MoveSelectionUp();
        } else if (_hasDroppedPenguin && verticalInput < -0.25f && Mathf.Abs(verticalInput) > Mathf.Abs(horizontalInput) && Time.time - _timeOfLastSelectionChange > ButtonSelectCooldown) {
            MoveSelectionDown();
        }
        
    }

    private void MoveSelectionLeft() {
        _timeOfLastSelectionChange = Time.time;
        if (CurrentHatButtonSelection == -1) {
            CurrentButtonSelection = Mathf.Max(0, CurrentButtonSelection - 1);
        }
        else {
            CurrentHatButtonSelection = Mathf.Max(CurrentHatButtonSelection - 1, 0);
        }

        UpdateButtonSelectionDisplay();
    }

    private void MoveSelectionRight() {
        _timeOfLastSelectionChange = Time.time;
        if (CurrentHatButtonSelection == -1) {
            CurrentButtonSelection = Mathf.Min(Buttons.Count - 1, CurrentButtonSelection + 1);
        }
        else {
            CurrentHatButtonSelection = Mathf.Min(CurrentHatButtonSelection + 1, _hatButtons.Count - 1);
        }

        UpdateButtonSelectionDisplay();
    }

    private void MoveSelectionUp() {
        _timeOfLastSelectionChange = Time.time;
        if (CurrentHatButtonSelection == -1) {
            CurrentHatButtonSelection = _hatButtons.Count - 1;
            _stashedButtonSelection = CurrentButtonSelection;
            CurrentButtonSelection = -1;
        }
        else {
            CurrentHatButtonSelection -= 3;
            if (CurrentHatButtonSelection < 0) {
                CurrentHatButtonSelection += 3;
            }
        }
        UpdateButtonSelectionDisplay();
    }

    private void MoveSelectionDown() {
        if (CurrentHatButtonSelection == -1) return;
        _timeOfLastSelectionChange = Time.time;
        CurrentHatButtonSelection += 3;
        if (CurrentHatButtonSelection >= _hatButtons.Count) {
            CurrentButtonSelection = _stashedButtonSelection;
            CurrentHatButtonSelection = -1;
        }
        UpdateButtonSelectionDisplay();
    }

    private void UpdateButtonSelectionDisplay() {
        for (int i = 0; i < Buttons.Count; i++) {
            Buttons[i].SetSelected(i == CurrentButtonSelection);
        }
        for (int i = 0; i < _hatButtons.Count; i++) {
            _hatButtons[i].SetSelected(i == CurrentHatButtonSelection);
        }
    }

    public void GoToCustomize() {
        StartMove(BackgroundPositionCustomize);
    }

    public void GoToMain() {
        StartMove(BackgroundPositionMain);
    }

    public void GoToLeaderboard() {
        StartMove(BackgroundPositionLeaderboard);
    }

    public void GoToGameplay() {
        StartMove(0);
        WhiteCoverController.DoMenuToGame();
        Invoke(nameof(GoToGameplayScene), 1.1f);
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void TitleHit() {
        AudioController.Instance.TitleHit();
    }
    public void UIClick() {
        AudioController.Instance.UIButtonClick();
    }
    public void HatClick() {
        AudioController.Instance.UIHatClick();
    }
    public void TitleHitThe() {
        AudioController.Instance.TitleStabThe();
    }
    public void TitleHitInevitable() {
        AudioController.Instance.TitleStabInevitable();
    }
    public void TitleHitSnowman() {
        AudioController.Instance.TitleStabSnowman();
    }
    public void UIButtonSelect() {
        AudioController.Instance.UIButtonSelect();
    }

    private void StartMove(float target) {
        CurrentHatButtonSelection = -1;
        if (CurrentButtonSelection == -1) {
            CurrentButtonSelection = 0;
            UpdateButtonSelectionDisplay();
        }
        _timeOfLastBackgroundPositionChange = Time.time;
        _backgroundPositionStart = _backgroundPositionCur;
        if (Mathf.Approximately(_backgroundPositionEnd, target)) {
            _backgroundPositionEnd = 0;
        } else {
            _backgroundPositionEnd = target;
        }
    }

    private void GoToGameplayScene() {
        SceneManager.LoadScene("Gameplay");
    }

    public void RegisterHatButton(MenuButtonController newHatButton) {
        _hatButtons.AddIfUnique(newHatButton);
    }
    
}
