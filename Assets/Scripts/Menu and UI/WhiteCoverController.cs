using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhiteCoverController : MonoBehaviour {

    public Image Image;

    public AnimationCurve MenuIntroCurve;
    public AnimationCurve MenuToGame;
    public float SmoothTime;
    
    private AnimationCurve _currentCurve;

    private float _alphaStart;
    private float _alphaEnd;
    private float _alphaTarget;
    private float _alphaCur;
    private float _alphaVel;

    private float _timeOfLastChange = -20f;

    private void Awake() {
        _alphaStart = _alphaEnd = _alphaTarget = _alphaCur = 1f;    // Start whited out.
    }
    
    private void Update() {
        if (_currentCurve == null) return;
        _alphaTarget = Mathf.LerpUnclamped(_alphaStart, _alphaEnd, _currentCurve.Evaluate(Time.time - _timeOfLastChange));
        _alphaCur = Mathf.SmoothDamp(_alphaCur, _alphaTarget, ref _alphaVel, SmoothTime);
        Image.color = new Color(1f, 1f, 1f, _alphaCur);
    }

    public void DoMenuIntro() {
        _timeOfLastChange = Time.time;
        _alphaStart = _alphaCur;
        _alphaEnd = 0f;
        _currentCurve = MenuIntroCurve;
    }
    
    public void DoMenuToGame() {
        _timeOfLastChange = Time.time;
        _alphaStart = _alphaCur;
        _alphaEnd = 1f;
        _currentCurve = MenuIntroCurve;
    }

}
