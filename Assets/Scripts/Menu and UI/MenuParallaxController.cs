using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuParallaxController : MonoBehaviour {

    public RectTransform ObservedRT;
    public Vector2 MotionMultiplier;

    private RectTransform _observedParentRT;

    public bool MovesRT = true;
    public bool MovesTransform = false;

    private RectTransform _myRT;
    private Transform _myTransform;
    private Vector2 _lastObservedPosition;

    private void Start() {
        _myRT = GetComponent<RectTransform>();
        _myTransform = transform;
        _observedParentRT = ObservedRT.parent.GetComponent<RectTransform>();
        _lastObservedPosition = ObservedRT.anchoredPosition + _observedParentRT.anchoredPosition;
    }
    
    private void Update() {
        Vector2 curPos = ObservedRT.anchoredPosition + _observedParentRT.anchoredPosition;
        Vector2 diff = curPos - _lastObservedPosition;
        _lastObservedPosition = curPos;
        if (MovesRT) {
            _myRT.anchoredPosition += new Vector2(diff.x * MotionMultiplier.x, diff.y * MotionMultiplier.y);
        }

        if (MovesTransform) {
            _myTransform.Translate(new Vector2(diff.x * MotionMultiplier.x, diff.y * MotionMultiplier.y));
        }
    }

}
