using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class AudioEventCaller : MonoBehaviour {

    public UnityEvent Event;

    public void InvokeAudioEvent() {
        Event?.Invoke();
    }

}
