using UnityEngine;

public class AnimationValueSetter : MonoBehaviour {
    public Animator Animator;
    public string Key;
    private int _keyHash;

    public void SetFloat(float value) {
        if (_keyHash == 0) {
            _keyHash = Animator.StringToHash(Key);
        }
        Animator.SetFloat(_keyHash, value);
    }
}
