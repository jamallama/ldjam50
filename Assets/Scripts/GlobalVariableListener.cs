using System;
using UnityEngine;
using UnityEngine.Events;

public class GlobalVariableListener : MonoBehaviour {
    public GlobalVariable Variable;

    [Serializable]
    public class ValueChangedCallback : UnityEvent<float> { }

    public ValueChangedCallback ValueChanged;
    public ValueChangedCallback ValueChangedPercent;
    public UnityEvent ReachedMinimum;
    public UnityEvent ReachedMaximum;

    private void Start() {
        if (Variable) {
            Variable.ValueChanged += OnValueChanged;
            Variable.ReachedMinimum += OnReachedMinimum;
            Variable.ReachedMaximum += OnReachedMaximum;
            OnValueChanged(Variable.Value);
            if (Variable.Value <= Variable.MinValue) {
                OnReachedMinimum();
            }
            if (Variable.Value >= Variable.MaxValue) {
                OnReachedMaximum();
            }
        }
    }

    private void OnValueChanged(float value) {
        ValueChanged.Invoke(value);
        ValueChangedPercent.Invoke(Mathf.InverseLerp(Variable.MinValue, Variable.MaxValue, value));
    }

    private void OnReachedMinimum() {
        ReachedMinimum.Invoke();
    }

    private void OnReachedMaximum() {
        ReachedMaximum.Invoke();
    }

    private void OnDestroy() {
        if (Variable) {
            Variable.ValueChanged -= OnValueChanged;
            Variable.ReachedMinimum -= OnReachedMinimum;
            Variable.ReachedMaximum -= OnReachedMaximum;
        }
    }
}