using UnityEngine;

public class RotateOverTime : MonoBehaviour {
    public Vector3 Speed = new Vector3(0, 0, -90);

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Speed * Time.deltaTime);
    }
}
