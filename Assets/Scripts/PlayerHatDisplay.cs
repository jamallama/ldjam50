using System.Linq;
using UnityEngine;

public class PlayerHatDisplay : MonoBehaviour {
    public HatDatabaseObject HatDB;
    private void Start() {
        var hat = HatDB.Hats.First(e => e.ID == HatSelection.GetSelectedHatID());
        if (hat.prefab != null) {
            Instantiate(hat.prefab, transform);
        }
    }
}
