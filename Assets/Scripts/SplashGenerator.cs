using UnityEngine;

public class SplashGenerator : MonoBehaviour {
    public ParticleSystem Splash;
    private void OnTriggerEnter(Collider other) {
        if (!other.CompareTag("Terrain")) return;
        
        Vector3 pos = other.transform.position;
        var splashTransform = Splash.transform;
        pos.y = splashTransform.position.y;
        splashTransform.position = pos;
        
        Splash.Play();
    }
}
