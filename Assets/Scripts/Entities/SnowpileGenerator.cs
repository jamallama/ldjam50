using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class SnowpileGenerator : MonoBehaviour {
    public float Radius;
    public SnowballCollectable SnowpilePrefab;
    public int SnowToGenerate = 10;
    public float TimeToGenerate = 0.05f;
    public AnimationCurve Concentration;
    public AnimationCurve AverageSize;
    public float SizeRange;
    public float TimeToGenerateHat = 60f;
    public HatGenerator HatGenerator;
    public Transform[] RandomHatTargets;

    private float _timeToNextGeneration;
    private float _timeToNextHatGeneration;
    
    private Hat hatToCollect;
    private Dictionary<GameObject, GameObject> platformToDroppedObjects = new Dictionary<GameObject, GameObject>();

    void Start() {
        GenerateSnow();
        _timeToNextHatGeneration = Time.time + TimeToGenerateHat;
    }

    private void GenerateSnow() {
        _timeToNextGeneration = Time.time + TimeToGenerate;

        Vector2 xz = Random.insideUnitCircle;
        float distance = xz.magnitude;
        xz = xz.normalized * Concentration.Evaluate(distance);
        xz *= Radius;
        Vector3 pos = transform.position + new Vector3(xz.x, 0, xz.y);
        
        // Grab nearest platform that isn't sinking.
        List<IcePlatformController> platforms = IcePlatformManager.Instance.GetActivePlatforms().OrderBy(e => Vector3.Distance(pos, e.transform.position)).ToList();
        IcePlatformController selectedPlatform = platforms.FirstOrDefault(e => e.CanSink && !e.IsSinking);

        if (selectedPlatform == null) return;
        GameObject colliderObject = selectedPlatform.ColliderObject;
        
        if (platformToDroppedObjects.ContainsKey(colliderObject)) {
            var existingObj = platformToDroppedObjects[colliderObject];
            if (existingObj) {
                return;
            }
        }
        Vector3 center = colliderObject.transform.position;
        pos.x = center.x;
        pos.z = center.z;

        // Don't spawn on already-sinking platforms.
        if (colliderObject.transform.parent != null) {
            IcePlatformController platformController = colliderObject.transform.parent.GetComponent<IcePlatformController>();
            if (platformController != null) {
                if (platformController.IsSinking) {
                    return;
                }
            }
        }
        
        if (Time.time > _timeToNextHatGeneration && hatToCollect == null) {
            bool useHatPosition = Random.Range(0, 2) == 0;
            if (useHatPosition) {
                pos = RandomHatTargets[Random.Range(0, RandomHatTargets.Length)].position;
            }
            hatToCollect = HatGenerator.GenerateHat(pos, transform);
            if (hatToCollect != null) {
                _timeToNextHatGeneration = Time.time + TimeToGenerateHat;
                return;
            }
            _timeToNextHatGeneration = float.MaxValue;
        }

        SnowballCollectable snowball = Instantiate(SnowpilePrefab, pos, Quaternion.identity, transform);
        // snowball.GetComponent<Rigidbody>().velocity = Physics.gravity;
        snowball.Contents = AverageSize.Evaluate(distance) + Random.Range(-SizeRange, SizeRange);
        platformToDroppedObjects[colliderObject] = snowball.gameObject;
    }

    private void Update() {
        if (transform.childCount < SnowToGenerate && Time.time >= _timeToNextGeneration) {
            GenerateSnow();
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Vector3 pos = transform.position;
        for (int i = 0; i < 36; ++i) {
            Vector3 a = pos + Quaternion.Euler(0, i * 10, 0) * Vector3.forward * Radius;
            Vector3 b = pos + Quaternion.Euler(0, (i + 1) * 10, 0) * Vector3.forward * Radius;
            Gizmos.DrawLine(a, b);
        }
    }
}
