using Rewired;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerShootController : MonoBehaviour {
    private const string AimHorizontalControl = "AimHorizontal";
    private const string AimVerticalControl = "AimVertical";
    private const string ActionControl = "Action";

    public GlobalVariable SnowInventory;
    public float MouseReticuleDisplayHeight;
    
    [FormerlySerializedAs("JoystickAimingReticle")] [Header("Projectile")]
    public GameObject AimingReticle;
    public SnowballProjectile ProjectilePrefab;
    public float AimingDistance = 1;
    public float ProjectileSpeed = 10;
    public float LobHeight = 1;
    public float ProjectileSizeIncreaseSpeed = 1;
    public float StartProjectileSize = 1;
    public float MaxProjectileSize = 10;
    public float ReticleSize = 0.4f;
    
    private Player _playerControls;
    public Vector3 AimDirection => _aimDirection;
    private Vector3 _aimDirection;
    private float _projectileSize;
    private bool _useMouseAimControls;

    public AnimationCurve AngleCheatToSnowmanByDistance;

    public AnimationCurve AimLineScaleByMouseDistance;
    public AnimationCurve AimLineScaleByStickMagnitude;
    public SpriteRenderer AimLineSpriteRenderer;
    public Gradient AimLineColorByTime;
    public float AimLineColorChangeDuration;
    private float _aimLineTimer;
    
    [Header("Dressing")]
    public BulletParticles ParticlePrefab;
    
    private void Awake() {
        SnowInventory.Value = SnowInventory.InitialValue;
        _playerControls = ReInput.players.GetPlayer(0);
    }

    void Update() {
        if (_playerControls.GetButtonDown(ActionControl)) {
            StartProjectile();
        }
        if (_playerControls.GetButton(ActionControl)) {
            IncreaseProjectileSize();
        }
        if (_playerControls.GetButtonUp(ActionControl)) {
            TryShoot();
        }

        if (_playerControls.controllers.GetLastActiveController().type == ControllerType.Mouse) {
            _useMouseAimControls = true;
        } else if (_playerControls.controllers.GetLastActiveController().type == ControllerType.Joystick) {
            _useMouseAimControls = false;
        }

        if (_useMouseAimControls) {
            Plane p = new Plane(Vector3.up, new Vector3(0, MouseReticuleDisplayHeight, 0));
            // ReSharper disable once PossibleNullReferenceException
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            p.Raycast(ray, out float enter);

            Vector3 dir = ray.GetPoint(enter) - transform.position;
            AimingReticle.transform.localScale = new Vector3(AimLineScaleByMouseDistance.Evaluate(dir.magnitude), 1f, 1f);
            AdjustAim(dir);
        } else {
            Vector3 aimDir = Vector3.zero;
            aimDir.x = _playerControls.GetAxis(AimHorizontalControl);
            aimDir.z = _playerControls.GetAxis(AimVerticalControl);
            AimingReticle.transform.localScale = new Vector3(AimLineScaleByStickMagnitude.Evaluate(aimDir.magnitude), 1f, 1f);
            AdjustAim(aimDir);
        }

        _aimLineTimer += Time.deltaTime;
        AimLineSpriteRenderer.color = AimLineColorByTime.Evaluate((_aimLineTimer / AimLineColorChangeDuration) % 1f);

    }
    
    private void AdjustAim(Vector3 aimDir) {
        _aimDirection += aimDir;
        _aimDirection.Normalize();
        AimingReticle.transform.localPosition = _aimDirection * AimingDistance;
        
        AimingReticle.transform.rotation = Quaternion.identity;
        float angle = -MathUtilities.RotationToLookAtVector(Vector2.right, new Vector2(aimDir.x, aimDir.z));
        AimingReticle.transform.Rotate(0f, angle, 0f);
    }

    private void StartProjectile() {
        if (SnowInventory.Value < StartProjectileSize) return;

        _projectileSize = StartProjectileSize;
    }

    private void IncreaseProjectileSize() {
        if (_projectileSize <= 0 || SnowInventory.Value <= 0) return;
        
        _projectileSize += Time.deltaTime * ProjectileSizeIncreaseSpeed;
        AimingReticle.transform.localScale = Vector3.one * (ReticleSize * StartProjectileSize); //(ReticleSize * StartProjectileSize);
        if (_projectileSize > MaxProjectileSize || _projectileSize > SnowInventory.Value) {
            TryShoot();
        }
    }

    private void TryShoot() {
        if (_projectileSize <= 0) {
            // SFX
            AudioController.Instance.OutOfSnow();

            return;
        }
        
        Vector3 aimPos = _aimDirection;

        // If the player is aiming very near the snowman, cheat to make it a perfect shot.
        Vector3 vecToSnowman = GameController.Instance.Snowman.transform.position - transform.position;
        float distToSnowman = vecToSnowman.magnitude;
        float angleToSnowman = Vector3.Angle(aimPos, vecToSnowman);
        if (angleToSnowman < AngleCheatToSnowmanByDistance.Evaluate(distToSnowman)) {
            aimPos = vecToSnowman;
        }
        
        aimPos.y = 0f;

        FireProjectile(aimPos);
        
    }

    private void FireProjectile(Vector3 aimPos) {
        var projectile = Instantiate(ProjectilePrefab, transform.position + aimPos.normalized, Quaternion.identity);
        aimPos.Normalize();
        projectile.Contents = StartProjectileSize; //_projectileSize;
        projectile.Rigidbody.velocity = aimPos * ProjectileSpeed;
        _projectileSize = 0;
        AimingReticle.transform.localScale = Vector3.one * (ReticleSize * StartProjectileSize);
        
        if (ParticlePrefab != null) {
            // SFX
            AudioController.Instance.ThrowSnowballSFX(gameObject);

            BulletParticles particles = Instantiate(ParticlePrefab, projectile.transform.position, projectile.transform.rotation);
            particles.SetFollowedTransform(projectile.transform);
            particles.StartParticles();
        }
    }
}
