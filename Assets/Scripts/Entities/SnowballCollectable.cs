using System;
using UnityEngine;

public class SnowballCollectable : MonoBehaviour {
    public GlobalVariable SnowInventory;

    public float MinContents = 1;
    public float Contents = 10;
    public float DecaySpeed = 1f;
    public float DecaySpeedPerSecond = 0.05f;

    public AnimationCurve StartScaleCurve;
    private float _lifeTimer;

    public GameObject CollectionEffect;

    public void Start() {
        transform.localScale = Vector3.one * Contents * StartScaleCurve.FirstValue();
    }

    private void Update() {
        if (_lifeTimer < StartScaleCurve.Duration()) {
            _lifeTimer += Time.deltaTime;
            transform.localScale = Vector3.one * (Contents * StartScaleCurve.Evaluate(_lifeTimer));
        } else {
            DecaySpeed += DecaySpeedPerSecond * Time.deltaTime;
            Contents -= DecaySpeed * Time.deltaTime;
            if (Contents <= MinContents) {
                Destroy(gameObject);
            } else {
                transform.localScale = Vector3.one * Contents;
            }
        }
    }

    public void OnTriggerStay(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            // SFX
            AudioController.Instance.SnowPickupSFX();
            
            if (Contents > 0) {
                Instantiate(CollectionEffect, transform.position, Quaternion.identity);
            }

            float toAdd = Contents;
            if (Contents - toAdd < MinContents) {
                toAdd = Contents;
                Contents = 0;
            }
            SnowInventory.Add(toAdd);
            Contents -= toAdd;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Respawn")) {
            Destroy(gameObject, 0.5f);
        }
    }
}
