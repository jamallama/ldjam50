using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

public class CatEnemy : MonoBehaviour {
    public SpriteRenderer Sprite;
    public FireballProjectile ProjectilePrefab;
    public Animator Animator;
    public Sprite IdleSprite;
    public Sprite AttackSprite;
    public Sprite HurtSprite;

    public enum MovementBehavior {
        Meandering,
        Basic,
        BigAndToughSnowmanKiller
    }
    
    [Serializable]
    public struct EnemyConfig {
        public float Speed;
        [Tooltip("When determining the direction to go (angle from the direction towards the snowman), randomly pick a value from this curve")]
        public AnimationCurve AngleCurve;
        public float MeltStrength;
        public float RangeForAttackingSnowman;
        public MovementBehavior Behavior;
        public int Health;
        public float ShootFrequency;
        public float ProjectileScale;
        public int ProjectileDamage;
    }

    public EnemyConfig Config;

    [Header("Movement")]
    public GameObject DebugWanderDestinationPrefab;
    public bool DebugShowCurrentWanderLocation;
    public float MinWanderDistance;
    public float MaxWanderDistance;
    public float ArrivalTolerance;

    [Header("Shooting")] 
    public Transform AimTransform;
    public Transform ProjectileSpawner;
    public float AttackAnimationLength;
    public float HurtAnimationLength;
    
    private Vector3 currentWanderLocation;
    private GameObject lastLocationObject;
    private Rigidbody rb;
    private Snowman snowman;
    private float currentHealth;
    private bool firstWanderLocationPicked;
    private float timeSinceLastShot = 5f;

    [Header("Damage Taken")]
    public FXController DamageTakenFX;

    [Header("Death")]
    public List<GameObject> ObjectsDroppedOnDeath; 

    [Header("Water Shadow")]
    public Transform WaterShadow;
    public Vector3 WaterShadowBaseScale;
    public AnimationCurve WaterShadowScaleByPlayerY;
    public Transform ShadowcasterTransform;
    public Vector3 ShadowcasterBaseScale;
    public AnimationCurve ShadowcasterScaleByPlayerY;

    private void Awake() {
        rb = GetComponent<Rigidbody>();
    }

    private void Start() {
        currentHealth = Config.Health;
        snowman = GameController.Instance.Snowman;
        EnemyManager.Instance.RegisterCatEnemy(this);

        // SFX
        // Jason added to keep track of number of enemies for music loop queue
        if (AudioController.Instance != null) { AudioController.Instance.numEnemies++; }
    }

    private void Update() {
        UpdateAim();

        // See if we have arrived to our target
        if ((transform.position - currentWanderLocation).magnitude <= ArrivalTolerance || !firstWanderLocationPicked) {
            firstWanderLocationPicked = true;
            PickNextWanderLocation();
        }
        
        Move(DirectionToTarget(transform.position, currentWanderLocation));

        timeSinceLastShot += Time.deltaTime;
        TryShoot();
        
        WaterShadow.position = new Vector3(WaterShadow.position.x, -.6f, WaterShadow.position.z);
        WaterShadow.localScale = WaterShadowBaseScale * WaterShadowScaleByPlayerY.Evaluate(transform.position.y);
        ShadowcasterTransform.localScale = ShadowcasterBaseScale * ShadowcasterScaleByPlayerY.Evaluate(transform.position.y);
    }

    private static Vector3 DirectionToTarget(Vector3 origin, Vector3 target) {
        return (target - origin).normalized;
    }
    
    private void Move(Vector3 moveDir) {
        Vector3 velocity = rb.velocity;
        velocity.x = moveDir.x * Config.Speed;
        velocity.z = moveDir.z * Config.Speed;
        rb.velocity = velocity;
        
        // Face the correct direction
        Sprite.flipX = moveDir.x >= 0;
    }

    private void UpdateAim() {
        Assert.IsNotNull(snowman, "The snowman was never set for this enemy!");

        Vector3 targetPosition = snowman.transform.position;
        Vector3 lookPosition = targetPosition;
        Vector3 aimPosition = AimTransform.position;
        lookPosition.y = aimPosition.y;
        
        // Aim at the snowman, of course
        AimTransform.LookAt(lookPosition);
    }

    private void TryShoot() {
        // Don't shoot if we recently shot
        if (timeSinceLastShot < Config.ShootFrequency) return;
        // Don't shoot if we are too far away from the snowman
        if ((snowman.transform.position - transform.position).magnitude > Config.RangeForAttackingSnowman) return;

        timeSinceLastShot = 0;
        Shoot();
    }
    
    [Button]
    private void Shoot() {
        FireballProjectile shot = Instantiate(ProjectilePrefab, ProjectileSpawner.transform.position, Quaternion.identity, EnemyManager.Instance.SpawnBucket);
        shot.DamageAmount = Config.ProjectileDamage;
        shot.transform.localScale = new Vector3(Config.ProjectileScale, Config.ProjectileScale, Config.ProjectileScale);
        StartCoroutine(PlayShootAnimation());
    }

    private IEnumerator PlayShootAnimation() {
        Animator.Play("Cat Attack");
        Sprite.sprite = AttackSprite;
        yield return new WaitForSeconds(AttackAnimationLength);
        Animator.Play("Cat Idle");
        Sprite.sprite = IdleSprite;
    }

    public void PickNextWanderLocation() {
        currentWanderLocation = GetNextWanderLocation();
        
        // Debug tool to display the current destination
        if (DebugShowCurrentWanderLocation) {
            if (lastLocationObject != null) {
                Destroy(lastLocationObject);
            }
            lastLocationObject = Instantiate(DebugWanderDestinationPrefab, currentWanderLocation, Quaternion.identity,
                EnemyManager.Instance.SpawnBucket);
        }
    }

    public void Damage(float damageAmount) {
        currentHealth -= damageAmount;
        DamageTakenFX.TriggerWithArgs(new FXArgs {InputVector = Vector2.up});
        if (currentHealth <= 0) {
            Die();
        } else {
            StartCoroutine(PlayHurtAnimation());
        }
    }

    private IEnumerator PlayHurtAnimation() {
        Animator.Play("Cat Attack");
        Sprite.sprite = HurtSprite;
        yield return new WaitForSeconds(HurtAnimationLength);
        Animator.Play("Cat Idle");
        Sprite.sprite = IdleSprite;
    }
    
    private Vector3 GetNextWanderLocation() {
        Vector3 movementVector = Vector3.right * Random.Range(MinWanderDistance, MaxWanderDistance);
        
        // Randomly pick a value from the configured AngleCurve to determine the direction away from the snowman to go
        float angle = Config.AngleCurve.Evaluate(Random.Range(0, Config.AngleCurve.Duration()));
        angle += AimTransform.eulerAngles.y - 90;
        // Debug.Log("aim euler y: " + AimTransform.eulerAngles.y + ". Final angle: " + angle);
        movementVector = Quaternion.Euler(0, angle, 0) * movementVector;
        return transform.position + movementVector;
    }

    private void Die() {
        // SFX
        if (Config.Behavior == MovementBehavior.Basic) { AudioController.Instance.SmallCatExplosionSFX(gameObject); }
        if (Config.Behavior == MovementBehavior.Meandering) { AudioController.Instance.SmallCatExplosionSFX(gameObject); }
        if (Config.Behavior == MovementBehavior.BigAndToughSnowmanKiller) { AudioController.Instance.BigCatExplosionSFX(gameObject); }

        // Jason added to keep track of number of enemies for music loop queue
        if (AudioController.Instance != null) { AudioController.Instance.numEnemies--; }

        CameraController.Instance.ApplyCamshake(.8f);
        foreach (GameObject go in ObjectsDroppedOnDeath) {
            Instantiate(go, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }

    private void OnDestroy() { 
        EnemyManager.Instance.UnregisterCatEnemy(this);
    }
}
