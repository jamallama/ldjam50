using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hat : MonoBehaviour
{
    public HatObject HatObject;
    public HatInventory HatInventory;

    public void OnCollisionStay(Collision collision) {
        if (collision.gameObject.CompareTag("Player")) {
            HatInventory.AddHat(HatObject);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Respawn")) {
            Destroy(gameObject, 0.5f);
        }
    }
}
