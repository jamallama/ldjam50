using System;
using System.Linq;
using Rewired;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private const string MovementHorizontalControl = "MoveHorizontal";
    private const string MovementVerticalControl = "MoveVertical";
    private const string AimHorizontalControl = "AimHorizontal";
    private const string AimVerticalControl = "AimVertical";
    private const string ActionControl = "Action";
    private const string JumpControl = "Jump";
    private const string PauseControl = "Pause";

    public GlobalVariable SnowInventory;
    public GlobalVariable Deaths;

    [Header("Player config")]
    public float BaseSpeed;
    public float SnowLossOnRespawn = 0.9f;
    
    private Player playerControls;
    private Rigidbody rb;
    private Vector3 startPos;
    private int jumpCount;
    
    [Header("Jump")]
    public float JumpHoldDuration;
    public float MinJumpHold;
    public AnimationCurve JumpHeldCurve;
    public AnimationCurve JumpFallCurve;
    public bool isHoldingJump;
    public float targetYVelocity;
    public float curYVelocity;
    private float yVelVel;
    public float ySmoothTime;
    public float timeIntoJumpHold;
    public float timeIntoJumpFall;
    public int maxJumpCount = 2;
    
    public bool IsGrounded;
    public GroundedDetector GroundedDetector;
    
    
    public Action BecomesGroundedAction;
    public Action StartsJumpAction;
    public Action StartsDoubleJumpAction;

    public GameObject PlayerArtRoot;

    [Header("Respawning")]
    public AnimationCurve RespawnTimeByDistance;
    public AnimationCurve RespawnMovementCurve;
    private Vector3 _respawnTweenStartPoint;
    private Vector3 _respawnTweenEndPoint;
    private Vector3 _respawnActualPoint;
    private bool _isRespawning;
    private float _respawnTimer;
    private float _respawnTime;

    public GameObject SplashEffectPrefab;

    [Header("Water Shadow")]
    public Transform WaterShadow;
    public Vector3 WaterShadowBaseScale;
    public AnimationCurve WaterShadowScaleByPlayerY;
    public Transform ShadowcasterTransform;
    public Vector3 ShadowcasterBaseScale;
    public AnimationCurve ShadowcasterScaleByPlayerY;
    
    
    private void Awake() {
        startPos = transform.position;
        playerControls = ReInput.players.GetPlayer(0);
        rb = GetComponent<Rigidbody>();
        targetYVelocity = curYVelocity = JumpFallCurve.LastValue();
    }

    void Update() {

        if (_isRespawning) {
            UpdateRespawn();
            return;
        }
        
        UpdateJump();
        
        Vector3 moveDir = rb.velocity;

        Vector2 moveVec = new Vector2(playerControls.GetAxis(MovementHorizontalControl), playerControls.GetAxis(MovementVerticalControl));
        if (moveVec.magnitude > 1) {
            moveVec.Normalize();
        }
        
        moveDir.x = moveVec.x;
        moveDir.z = moveVec.y;
        
        Move(moveDir);

        bool wasGrounded = IsGrounded;
        IsGrounded = GroundedDetector.IsGrounded();
        if (!wasGrounded && IsGrounded) {
            // SFX
            AudioController.Instance.PenguinLandSFX();

            jumpCount = 0;
            BecomesGroundedAction?.Invoke();
        }
        if (IsGrounded) {
            targetYVelocity = 0f;
            timeIntoJumpFall = 0f;
        }

        WaterShadow.position = new Vector3(WaterShadow.position.x, -.6f, WaterShadow.position.z);
        WaterShadow.localScale = WaterShadowBaseScale * WaterShadowScaleByPlayerY.Evaluate(transform.position.y);
        ShadowcasterTransform.localScale = ShadowcasterBaseScale * ShadowcasterScaleByPlayerY.Evaluate(transform.position.y);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Respawn") && !_isRespawning) {
            StartRespawn();
        }
    }

    private void UpdateJump() {
        if (playerControls.GetButtonDown(JumpControl)) {
            TryStartJump();
        }

        if (isHoldingJump) {
            timeIntoJumpHold += Time.deltaTime;

            if (!playerControls.GetButton(JumpControl)) {
                isHoldingJump = false;
            }
            
            if (timeIntoJumpHold > JumpHoldDuration) {
                isHoldingJump = false;
            }
        }
        
        if (isHoldingJump) {
            timeIntoJumpFall = 0f;
            targetYVelocity = JumpHeldCurve.Evaluate(timeIntoJumpHold);
        }
        else {
            timeIntoJumpHold = 0f;
            timeIntoJumpFall += Time.deltaTime;
            targetYVelocity = JumpFallCurve.Evaluate(timeIntoJumpFall);
        }

        curYVelocity = Mathf.SmoothDamp(curYVelocity, targetYVelocity, ref yVelVel, ySmoothTime);
    }
    
    private void Move(Vector3 moveDir) {
        Vector3 velocity = rb.velocity;
        velocity.x = moveDir.x * BaseSpeed;
        velocity.z = moveDir.z * BaseSpeed;
        velocity.y = curYVelocity;
        rb.velocity = velocity;
    }

    private void TryStartJump() {
        if (!GroundedDetector.IsGrounded() && jumpCount >= maxJumpCount) return;

        if (jumpCount == 0) {
            // SFX
            AudioController.Instance.JumpSFX(gameObject);

            StartsJumpAction?.Invoke();
        } else if (jumpCount == 1) {
            // SFX
            AudioController.Instance.DoubleJumpSFX(gameObject);

            StartsDoubleJumpAction?.Invoke();
        }
        isHoldingJump = true;
        curYVelocity = JumpHeldCurve.FirstValue();
        ++jumpCount;
    }

    private void StartRespawn() {
        var myTransform = transform;
        var pos = myTransform.position;
        SnowInventory.Multiply(1 - SnowLossOnRespawn);
        Deaths.Add(1);
        
        // SFX
        AudioController.Instance.SplashSFX(gameObject);

        Instantiate(SplashEffectPrefab,  new Vector3(pos.x, -0.35f, pos.z), Quaternion.identity);
        
        var nearbyPlatforms = Physics.OverlapSphere(pos, 5, LayerMask.GetMask("Terrain")).ToList();
            
        // Don't respawn on already-sinking platforms.
        for (int i = nearbyPlatforms.Count - 1; i >= 0; i--) {
            if (nearbyPlatforms[i].transform.parent == null) {
                nearbyPlatforms.RemoveAt(i);
                continue;
            }
            IcePlatformController platformController = nearbyPlatforms[i].transform.parent.GetComponent<IcePlatformController>();
            if (platformController == null) {
                nearbyPlatforms.RemoveAt(i);
                continue;
            }
            if (platformController.IsSinking) {
                nearbyPlatforms.RemoveAt(i);
            }
        }

        if (nearbyPlatforms.Count == 0) {
            _respawnActualPoint = startPos;
        } else {
            float minDist = float.MaxValue;
            var nearestPlatform = nearbyPlatforms[0];
            foreach (var platform in nearbyPlatforms) {
                float dist = (platform.transform.position - pos).sqrMagnitude;
                if (dist < minDist) {
                    minDist = dist;
                    nearestPlatform = platform;
                }
            }

            Vector3 targetPos = nearestPlatform.transform.position;
            targetPos.y = startPos.y + 1;
            _respawnActualPoint = targetPos;
        }

        _respawnTweenStartPoint = pos;
        _respawnTweenEndPoint = new Vector3(_respawnActualPoint.x, pos.y, _respawnActualPoint.z);
        _respawnTime = RespawnTimeByDistance.Evaluate(Vector3.Distance(_respawnTweenStartPoint, _respawnTweenEndPoint));
        _respawnTimer = 0f;
        PlayerArtRoot.SetActive(false);
        _isRespawning = true;

    }

    private void UpdateRespawn() {

        _respawnTimer += Time.deltaTime;
        var myTransform = transform;
        rb.velocity = Vector3.zero;
        myTransform.position = Vector3.LerpUnclamped(_respawnTweenStartPoint, _respawnTweenEndPoint, RespawnMovementCurve.Evaluate(_respawnTimer / _respawnTime));
        if (_respawnTimer > _respawnTime) CompleteRespawn();

    }

    private void CompleteRespawn() {
        
        _isRespawning = false;
        PlayerArtRoot.SetActive(true);
        transform.position = _respawnActualPoint;

    }
    
}
