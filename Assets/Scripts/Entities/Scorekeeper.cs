using System;
using TMPro;
using UnityEngine;

public class Scorekeeper : MonoBehaviour {
    public GlobalVariable SnowmanHealth;
    public GlobalVariable Timer;
    public GlobalVariable Deaths;
    public TextMeshProUGUI DeathsDisplay;
    public TextMeshProUGUI ScoreDisplay;

    private void Start() {
        Deaths.ValueChanged += OnDeathsChanged;
        Timer.Value = 0;
        Deaths.Value = 0;
        ScoreDisplay.text = FormatTimeString("00:00.000");
        DeathsDisplay.text = "0";
    }

    private void OnDestroy() {
        Deaths.ValueChanged -= OnDeathsChanged;
    }

    private void OnDeathsChanged(float value) {
        DeathsDisplay.text = Mathf.RoundToInt(value).ToString();
    }

    void Update() {
        if (SnowmanHealth.Value > 0) {
            Timer.Value += Time.deltaTime;
            string scoreDisplayString = TimeSpan.FromSeconds(Timer.Value).ToString(@"mm\:ss\.fff");
            string scoreDisplay2 = FormatTimeString(scoreDisplayString);
                                   
            ScoreDisplay.text = scoreDisplay2;
        }
    }

    private string FormatTimeString(string input) {
        return input.Substring(0, input.Length - 3) + 
                               "<size=0.85em>" + input.Substring(input.Length - 3, 1) + "</size>"  +
                               "<size=0.7225em>" + input.Substring(input.Length - 2, 1) + "</size>" +
                               "<size=0.6141em>" + input.Substring(input.Length - 1, 1) + "</size>";
    }
    
}
