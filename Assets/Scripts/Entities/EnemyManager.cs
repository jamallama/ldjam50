using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyManager : MonoBehaviour {
    public static EnemyManager Instance;
    public Transform SpawnBucket;
    
    public GlobalVariable GameRunning;
    
    [Header("Spawner prefabs")]
    public IcePlatformController BasicSpawner;
    public IcePlatformController BigSpawner;
    public IcePlatformController MeanderingSpawner;

    private List<IcePlatformController> CatSpawnPlatforms = new List<IcePlatformController>();

    private List<CatEnemy> _activeCats = new List<CatEnemy>();

    [Serializable]
    public struct TimedSpawnerPlacementBehavior {
        public string Name;
        public float Duration;
        public float BasicSpawnerRelativeWeight;
        public float MeanderingSpawnerRelativeWeight;
        public float BigSpawnerRelativeWeight;
        public int AmountOfSpawnersToSpawn;
        public float MinDist;
        public float MaxDist;
    }

    [Tooltip("Each of these durations are evaluated sequentially over time, one after another")]
    public List<TimedSpawnerPlacementBehavior> SpawnBehaviors;
    
    private float timeUntilNextSpawn = 10f;

    public void RegisterSpawnerPlatform(IcePlatformController spawner) {
        CatSpawnPlatforms.Add(spawner);
    }
    
    public void UnRegisterSpawnerPlatform(IcePlatformController spawner) {
        CatSpawnPlatforms.Remove(spawner);
    }
    
    private void Awake() {
        Instance = this;
    }

    private void Start() {
        _currentPlacementBehavior = SpawnBehaviors[_placementBehaviorIndex];
        _durationTimeRemaining = _currentPlacementBehavior.Duration;
        _timeUntilNextSpawn = GetNextSpawnTime();
    }

    private int _placementBehaviorIndex;
    private float _durationTimeRemaining;
    private float _timeUntilNextSpawn;
    private TimedSpawnerPlacementBehavior _currentPlacementBehavior;
    
    private void Update() {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (GameRunning.Value == 0) {
            return;
        }
        
        _durationTimeRemaining -= Time.deltaTime;
        if (_durationTimeRemaining <= 0) {
            // We have exhausted the duration of the current behavior, so go to the next one
            _placementBehaviorIndex++;
            _currentPlacementBehavior = SpawnBehaviors[_placementBehaviorIndex];
            _durationTimeRemaining += _currentPlacementBehavior.Duration;
            _timeUntilNextSpawn = 0;
        } else {
            _timeUntilNextSpawn -= Time.deltaTime;
        }
        
        if (_timeUntilNextSpawn <= 0) {
            // Time to spawn another spawner
            _timeUntilNextSpawn += GetNextSpawnTime();
            SpawnSpawner(_currentPlacementBehavior);
        }
    }

    private float GetNextSpawnTime() {
        TimedSpawnerPlacementBehavior behavior = _currentPlacementBehavior;
        if (behavior.AmountOfSpawnersToSpawn == 0) {
            return float.MaxValue;
        }
        return behavior.Duration / behavior.AmountOfSpawnersToSpawn;
    }

    private void SpawnSpawner(TimedSpawnerPlacementBehavior behavior) {
        float totalWeight = behavior.BasicSpawnerRelativeWeight + behavior.BigSpawnerRelativeWeight
                                                                + behavior.MeanderingSpawnerRelativeWeight;
        float basicWeight = behavior.BasicSpawnerRelativeWeight / totalWeight;
        float bigWeight = behavior.BigSpawnerRelativeWeight / totalWeight;
        float meanderingWeight = behavior.MeanderingSpawnerRelativeWeight / totalWeight;

        GameObject spawnPrefab;
        float r = Random.Range(0f, 1f);
        if (r <= basicWeight) {
            Debug.Log("Spawning basic spawner at time " + Time.timeSinceLevelLoad);
            spawnPrefab = BasicSpawner.gameObject;
        } else if (r <= basicWeight + bigWeight) {
            Debug.Log("Spawning big spawner at time " + Time.timeSinceLevelLoad);
            spawnPrefab = BigSpawner.gameObject;
        } else {
            Debug.Log("Spawning meandering spawner at time " + Time.timeSinceLevelLoad);
            spawnPrefab = MeanderingSpawner.gameObject;
        }
        
        IcePlatformManager.Instance.SpawnNewCatSpawnPlatform(spawnPrefab, behavior.MinDist, behavior.MaxDist);
    }

    public void RegisterCatEnemy(CatEnemy enemy) {
        _activeCats.AddIfUnique(enemy);
    }

    public void UnregisterCatEnemy(CatEnemy enemy) {
        _activeCats.Remove(enemy);
    }

    public List<CatEnemy> GetCatEnemies() {
        return _activeCats;
    }
    
}
