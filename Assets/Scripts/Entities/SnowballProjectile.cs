using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnowballProjectile : MonoBehaviour {
    public GlobalVariable SnowInventory;
    public GlobalVariable SnowmanHealth;
    
    public float Contents = 10;
    public Rigidbody Rigidbody;

    public float DamageRadius;
    public float HealRadius;

    public float PreferredYValue;
    public AnimationCurve PushStrengthTowardPreferredYByY;
    public bool _hasEverHitTerrain;
    
    public GameObject ImpactEffectPrefab;

    public void Start() {
        if (SnowInventory.Value < Contents) {
            Contents = SnowInventory.Value;
            if (Contents <= 0) {
                Destroy(gameObject);
                return;
            }
        }
        transform.localScale *= Contents;
        SnowInventory.Value -= Contents;
    }

    private void Update() {
        if (_hasEverHitTerrain) return;
        Vector3 myPos = transform.position;
        float diffToPreferredY = PreferredYValue - myPos.y;
        transform.Translate(0f, PushStrengthTowardPreferredYByY.Evaluate(diffToPreferredY) * Time.deltaTime, 0f);
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Terrain")) {
            _hasEverHitTerrain = true;
            Rigidbody.useGravity = true;
            return;
        }
        
        if (collision.gameObject.CompareTag("Player")) {
            return;
        }
        
        if (collision.gameObject.CompareTag("Snowman")) {
            DoEffect();
        }
    }

    private void OnTriggerEnter(Collider collision) {
        if (collision.gameObject.CompareTag("Enemy")) {
            DoEffect();
        }    
    }

    private void DoEffect() {
        
        // SFX
        AudioController.Instance.SnowballImpactSFX(gameObject);

        Instantiate(ImpactEffectPrefab, transform.position, Quaternion.identity);
        
        Vector3 myPos = transform.position;
        
        // Heal Snowman if within radius
        float distToSnowman = Vector3.Distance(myPos, GameController.Instance.Snowman.transform.position);
        if (distToSnowman < HealRadius) {
            SnowmanHealth.Value += Contents;
        }
        
        // Damage cats if within radius
        List<CatEnemy> enemiesInRadius = EnemyManager.Instance.GetCatEnemies().Where(e => Vector3.Distance(myPos, e.transform.position) < DamageRadius).ToList();
        foreach (CatEnemy enemy in enemiesInRadius) {
            enemy.Damage(Contents);
        }
        
        Destroy(gameObject);
    }
}
