using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class HatInventory : ScriptableObject {
    private const string _filename = "hats.save";

    private string SavePath => Path.Combine(Application.persistentDataPath, _filename);

    public HatDatabaseObject HatDatabase;
    [NonSerialized]
    private List<HatObject> _hats = new List<HatObject>();
    private Dictionary<string, HatObject> HatMap;

    public static string SelectedHatID = "NoHat";
    public List<HatObject> Hats {
      get
      {
        Load();
        return _hats;
      }
    }

    public void AddHat(HatObject hat) {
      Load();
      if (_hats.Contains(hat)) return;
      _hats.Add(hat);
      Save();
    }

    private void Save() {
      string saveData = "";
      for (int i = 0; i < _hats.Count; i++) {
        saveData += _hats[i].ID + "\n";
      }
      File.WriteAllText(SavePath, saveData);
    }

    public void Load() {
      if (HatMap != null) {
        return;
      }

      HatMap = HatDatabase.Hats.ToDictionary(e => e.ID);

      if (File.Exists(SavePath)) {
        string[] lines = File.ReadAllLines(SavePath);
        foreach (var id in lines) {
          if (id.Length == 0 || !HatMap.ContainsKey(id)) continue;
          HatObject hat = HatMap[id];
          _hats.Add(hat);
        }
      }

      if (!Hats.Contains(HatMap["NoHat"])) {
        _hats.Add(HatMap["NoHat"]);
      }
    }
}
