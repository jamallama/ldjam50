using System.Linq;
using UnityEngine;

public class HatGenerator : MonoBehaviour {
  public Hat HatPrefab;
  public HatDatabaseObject HatDatabase;
  public HatInventory HatInventory;
  private HatObject _toGenerate;
  public HatObject ToGenerate => _toGenerate;

  void Awake() {
    var unownedHats = HatDatabase.Hats.Where(e => !HatInventory.Hats.Contains(e)).ToArray();
    if (unownedHats.Length > 0) {
      _toGenerate = unownedHats[Random.Range(0, unownedHats.Length)];
    }
  }

  public Hat GenerateHat(Vector3 dropPosition, Transform parent) {
    if (_toGenerate == null || HatInventory.Hats.Contains(_toGenerate)) {
      return null;
    }
    Hat hat = Instantiate(HatPrefab, dropPosition, Quaternion.identity, parent);
    hat.GetComponent<Hat>().HatObject = _toGenerate;
    hat.GetComponent<SpriteRenderer>().sprite = _toGenerate.Sprite;
    return hat;
  }
}
