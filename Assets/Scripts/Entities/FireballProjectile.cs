using System;
using UnityEngine;

public class FireballProjectile : MonoBehaviour {
    public GlobalVariable SnowmanHealth;

    public float Speed = 10;
    public float DamageAmount = 10;
    public float AimHeight;
    public Rigidbody Rigidbody;

    public void Start() {
        Vector3 aimDirection = (GameController.Instance.Snowman.transform.position - transform.position).normalized;
        aimDirection.y += AimHeight;
        Rigidbody.velocity = aimDirection * Speed;
    }
    
    private void OnCollisionEnter(Collision collision) {
        Destroy(gameObject);
        if (collision.gameObject.CompareTag("Snowman")) {
            SnowmanHealth.Value -= DamageAmount;
        }
    }
}
