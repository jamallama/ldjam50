using System;
using UnityEngine;

public class Snowman : MonoBehaviour {
    public GlobalVariable SnowmanHealth;

    private void Awake() {
        SnowmanHealth.Value = SnowmanHealth.InitialValue;
    }

    // Update is called once per frame
    void Update() {
        SnowmanHealth.Value -= Time.deltaTime;
        if (SnowmanHealth.Value < 0) {
            enabled = false;
        }
    }
}
