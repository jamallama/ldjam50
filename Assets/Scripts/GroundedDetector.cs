using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedDetector : MonoBehaviour {
    
    [Header("Keep these the same length!")]
    public List<Vector3> StartOffsets;
    public List<Vector3> EndOffsets;
    public float OffsetScale = 1f;

    public LayerMask LayerMask;

    public bool IsGrounded() {

        bool hitTerrain = false;
        Vector3 tPos = transform.position;
        for (int i = 0; i < StartOffsets.Count; i++) {
            Physics.Linecast(tPos + StartOffsets[i] * OffsetScale, tPos + EndOffsets[i] * OffsetScale, out var hit, LayerMask);
            Collider hitCollider = hit.collider;
            if (hitCollider == null) continue;
            IcePlatformPenguinDetector detector = hitCollider.GetComponent<IcePlatformPenguinDetector>();
            if (detector != null) {
                detector.RegisterPenguinGroundedHere();
            }
            hitTerrain = hitTerrain || hit.collider.gameObject.layer == LayerIDs.Terrain;
        }
        return hitTerrain;
    }
    
}
