using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedLife : MonoBehaviour {

    public float Lifespan;
    private float _timeSoFar;
    
    private void Update() {
        _timeSoFar += Time.deltaTime;
        if (_timeSoFar > Lifespan) {
            Destroy(gameObject);
        }
    }

}
