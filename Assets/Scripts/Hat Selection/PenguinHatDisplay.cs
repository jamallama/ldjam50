using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PenguinHatDisplay : MonoBehaviour {

    public HatDatabaseObject HatData;
    public Transform HatScalerTransform;
    private GameObject _lastCreatedHat;

    private void OnEnable() {
        CreateHat();
        HatSelection.NewHatSelected += CreateHat;
    }

    private void CreateHat() {
        if (_lastCreatedHat != null) {
            Destroy(_lastCreatedHat);
        }
        HatObject hat = HatData.Hats.FirstOrDefault(e => e.ID == HatSelection.GetSelectedHatID());
        if (hat.prefab != null) {
            _lastCreatedHat = Instantiate(hat.prefab, HatScalerTransform);
        }
    }

    private void OnDestroy() {
        HatSelection.NewHatSelected -= CreateHat;
    }

}
