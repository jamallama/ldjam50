using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MenuHatDisplayer : MonoBehaviour {
    
    public Image BackgroundImage;
    public Sprite BackgroundSelectedSprite;
    public Sprite BackgroundNotSelectedSprite;

    public Image HatImage;

    public GameObject NotUnlockedObject;

    public GameObject Checkmark;

    private HatDatabaseObject _hatData;
    private HatInventory _hatInventory;
    private string _hatId;
    private HatObject _myHat;
    private bool _hatIsOwned;
    private bool _hatIsSelected;

    private bool _listeningForNewHat;

    [Header("Selection Effects")]
    public FXController BodyFX;
    public FXController CheckboxFX;

    [Header("Controller Support")]
    public MenuButtonController ButtonController;
    
    public void Init(HatDatabaseObject hatData, HatInventory hatInventory, string hatId) {
        _hatData = hatData;
        _hatInventory = hatInventory;
        _hatId = hatId;
        _myHat = _hatData.Hats.FirstOrDefault(e => e.ID == hatId);
        _hatIsOwned = hatInventory.Hats.Contains(_myHat);
        _hatIsSelected = HatSelection.GetSelectedHatID() == _hatId;
        if (_myHat == null) return;

        BackgroundImage.sprite = _hatId == HatSelection.GetSelectedHatID() ? BackgroundSelectedSprite : BackgroundNotSelectedSprite;

        if (_hatIsOwned) {
            HatImage.gameObject.SetActive(true);
            HatImage.sprite = _hatIsSelected ? _myHat.MenuSpriteIfSelected : _myHat.Sprite;
            NotUnlockedObject.SetActive(false);
        }
        else {
            HatImage.gameObject.SetActive(false);
            NotUnlockedObject.SetActive(true);
        }
        
        Checkmark.SetActive(_hatIsSelected);

        if (!_listeningForNewHat) {
            _listeningForNewHat = true;
            HatSelection.NewHatSelected += ReInit;
        }
        
        MenuManager.Instance.RegisterHatButton(ButtonController);
    }

    private void ReInit() {
        Init(_hatData, _hatInventory, _hatId);
    }
    
    public void ReceiveClick () {
        if (!_hatIsOwned) return;
        // SFX
        AudioController.Instance.UIHatClick();

        HatSelection.SetSelectedHatID(_hatId);
        BodyFX.Targets.Clear();
        BodyFX.Targets.Add(gameObject);
        BodyFX.Trigger();
        CheckboxFX.Targets.Clear();
        CheckboxFX.Targets.Add(Checkmark);
        CheckboxFX.Trigger();
    }

    private void OnDestroy() {
        HatSelection.NewHatSelected -= ReInit;
    }

}
