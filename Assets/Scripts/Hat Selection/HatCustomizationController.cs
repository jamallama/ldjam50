using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatCustomizationController : MonoBehaviour {

    public GameObject HatDisplayHolderTemplate;
    public HatInventory HatInventory;
    public HatDatabaseObject HatData;

    private void Start() {
        foreach (HatObject hat in HatData.Hats) {
            MenuHatDisplayer newDisplayer = Instantiate(HatDisplayHolderTemplate, HatDisplayHolderTemplate.transform.parent).GetComponentInChildren<MenuHatDisplayer>();
            newDisplayer.Init(HatData, HatInventory, hat.ID);
        }
        HatDisplayHolderTemplate.gameObject.SetActive(false);
    }

}
