using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BulletParticles : MonoBehaviour {

    public float TimeLingeredAfterTransformDies;
    public bool StopParticleEmissionWhenTransformDies;

    private Transform _followedTransform;
    private bool _transformWasEverSet;
    private bool _selfDestructScheduled;
    private float _selfDestructTimer;

    private List<ParticleSystem> _particleSystems;
    private List<ParticleSystem> ParticleSystems {
        get { return _particleSystems ??= GetComponentsInChildren<ParticleSystem>().ToList(); }
    }
    
    public void SetFollowedTransform(Transform t) {
        _followedTransform = t;
        _transformWasEverSet = true;
    }

    public void StartParticles() {
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>()) {
            ps.Play();
        }
    }

    private void Update() {

        bool followedTransformNull = _followedTransform == null;
        
        if (_transformWasEverSet && !_selfDestructScheduled && followedTransformNull) {
            if (StopParticleEmissionWhenTransformDies) {
                foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>()) {
                    ps.Stop();
                }
            }
            _selfDestructScheduled = true;
        }

        if (_selfDestructScheduled) {
            _selfDestructTimer += Time.deltaTime;
            if (_selfDestructTimer >= TimeLingeredAfterTransformDies) {
                Destroy(gameObject);
                return;
            }
        }

        if (followedTransformNull) return;

        transform.position = _followedTransform.position;
        transform.rotation = _followedTransform.rotation;

    }

}
