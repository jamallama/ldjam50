using UnityEngine;

public class IcePlatformCatDetector : MonoBehaviour {
    public IcePlatformController Controller;
    private void OnTriggerStay(Collider collision) {
        if (collision.gameObject.CompareTag("Enemy")) {
            CatEnemy enemy = collision.gameObject.GetComponent<CatEnemy>();
            Controller.OnCatIsClose(enemy);
        }
    }
    
    private void OnTriggerExit(Collider collision) {
        if (collision.gameObject.CompareTag("Enemy")) {
            Controller.OnCatLeft();
        }
    }
}
