﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class StringMorpher : MonoBehaviour {

	private TextMeshProUGUI text;

	public string AwakeString = "";
	
	private string MorphString = "``";
	private string MorphedPattern = "``";

	public float Duration = 0.75f;
	public AnimationCurve MorphCurve;
	private float timer;

	private float Progress => MorphCurve.Evaluate(timer / Duration);
	private int TotalSteps => (string.IsNullOrEmpty(startString)?0:startString.Length) + (string.IsNullOrEmpty(endString)?0:endString.Length);
	private int StepsExpected => (int) ((float) TotalSteps * Progress);
	private int totalStepsPerformed = 0;
	private float StartStepProgress => 1f - Mathf.Clamp01((float) startStringLettersRemaining.Count / startStringLetters.Count);
	private float EndStepProgress => 1f - Mathf.Clamp01((float) endStringLettersUnused.Count / endStringLetters.Count);

	private string CurrentString {
		get {
			StringBuilder sb = new StringBuilder("");
			foreach (int num in currentStringLetters) {
				sb.Append(CharFromNum(num));
			}
			return sb.ToString();
		}
	}
	
	private string startString;
	private string endString;

	private List<int> startStringLetters = new List<int>();
	private List<int> endStringLetters = new List<int>();
	private List<int> startStringLettersRemaining = new List<int>();
	private List<int> endStringLettersUnused = new List<int>();

	private List<int> currentStringLetters = new List<int>();

	private bool finalizedString = false;
	
	[Header("Testing")]
	public string TestString;
	public bool DoTest;


	private void Awake() {
		text = GetComponent<TextMeshProUGUI>();
		text.text = AwakeString;
	}

	private char CharFromNum(int num) {
		if (num < startString.Length) {
			return startString[num];
		} else {
			return endString[num - startString.Length];
		}
	}
	
	private void Update() {
		if (DoTest) {
			DoTest = false;
			StartMorph(TestString);
		}
		
		if (Progress < 1f) {
			timer += Time.deltaTime;
			while (totalStepsPerformed < StepsExpected) {
				DoStep();
			}
		} 
		
		// Guarantee we hit the final string.
		if (!finalizedString && Progress >= 1f) {
			text.text = MorphString.Replace(MorphedPattern, endString);
			finalizedString = true;
		}
	}

	private void DoStep() {
		if (StartStepProgress < EndStepProgress) {
			DoStartStep();
		} else {
			DoEndStep();
		}

		text.text = MorphString.Replace(MorphedPattern, CurrentString);
		totalStepsPerformed++;
	}

	private void DoStartStep() {
		int numToMove = startStringLettersRemaining[Random.Range(0, startStringLettersRemaining.Count)];
		startStringLettersRemaining.Remove(numToMove);
		currentStringLetters.Remove(numToMove);
	}

	private void DoEndStep() {
		if (endStringLettersUnused.Count <= 0) return;
		if (endStringLetters.Count <= 0) return;
		int numToMove = endStringLettersUnused[Random.Range(0, endStringLettersUnused.Count)];
		int numBefore = endStringLetters[0] - 1;
		int numAfter = endStringLetters[endStringLetters.Count - 1] + 1;
		endStringLettersUnused.Remove(numToMove);
		int idxBefore = 0;
		for (int i = 0; i < currentStringLetters.Count; i++) {
			int num = currentStringLetters[i];
			if (num >= startString.Length && num < numToMove) {
				idxBefore = i;
				numBefore = currentStringLetters[i];
			}
		}
		int idxAfter = currentStringLetters.Count;
		for (int i = currentStringLetters.Count - 1; i >= 0; i--) {
			int num = currentStringLetters[i];
			if (num > numToMove) {
				idxAfter = i;
				numAfter = currentStringLetters[i];
			}
		}
		if (idxBefore < currentStringLetters.Count) {
			idxBefore++;
		}
		if (idxAfter < currentStringLetters.Count) {
			idxAfter++;
		}

		float pct = ((float)numToMove - numBefore) / ((float)numAfter - numBefore);
		int range = idxAfter - idxBefore;
		int insertIdx = idxBefore + (int)((float) range * pct);
		currentStringLetters.Insert(insertIdx, numToMove);
	}

	public void StartMorph(string newEndString) {
		startString = CurrentString;
		endString = newEndString;
		startStringLetters.Clear();
		startStringLettersRemaining.Clear();
		currentStringLetters.Clear();
		for (int i = 0; i < startString.Length; i++) {
			startStringLetters.Add(i);
			startStringLettersRemaining.Add(i);
			currentStringLetters.Add(i);
		}

		endStringLetters.Clear();
		endStringLettersUnused.Clear();
		for (int i = currentStringLetters.Count; i < currentStringLetters.Count + endString.Length; i++) {
			endStringLetters.Add(i);
			endStringLettersUnused.Add(i);
		}

		timer = 0f;
		totalStepsPerformed = 0;
		finalizedString = false;
	}
	
}
