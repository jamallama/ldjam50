using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipDisplayer : MonoBehaviour {
    
    public List<string> Tips;

    public StringMorpher Morpher;

    private int _currentTip = -1;

    private void OnEnable() {
        Tips.Shuffle();
        IncrementTip();
        RefreshDisplay();
    }
    
    public void NextTip() {
        IncrementTip();
        RefreshDisplay();
    }

    private void IncrementTip() {
        _currentTip++;
        if (_currentTip >= Tips.Count) {
            _currentTip = 0;
        }
    }

    private void RefreshDisplay() {
        Morpher.StartMorph(Tips[_currentTip]);
    }

}
